<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title> LOGIN </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">      
	<meta http-equiv="Pragma" content="no-cache">
	
	<link rel="icon" type="image/png" href="/img/favicon.ico"/>
    <link rel="apple-touch-icon" href="/img/favicon.ico" />
	<!-- Css Styles -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
	
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<style>.card{padding:20px !important;}  a:hover{color:blue;}</style>
</head>

<body id="page-top">
 
	<section class="register_form">	
		<div class="container-fluid"> 
			<div class="row justify-content-md-center"> 
				<div class="col-md-6  login">
					<img src="{{asset('img/loginpage.jpeg')}}" class="img-fluid" alt=""> 
				</div>

				<div class="col-md-6 card login">        
					<div class="row">        
						<div class="col-sm-6">        
							<h3 class=""> LOGIN 
							<!-- @if(isset(Auth::user()->email)) {{Auth::user()->email}} @endif --> </h3>
						</div>
						<div class="col-sm-6 text-right">
							<a href="#">Forgot Password?</a>
						</div>
					</div> <!-- Row -->
					<hr />
				  <!--  To Show Alert Message -->
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul> @foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach </ul>
						</div>
					@endif

						@if ($message = Session::get('error'))
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif

						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
								<!--  / To Show Alert Message -->
					<form name="form1" method="post" action="/delegate/login"   class="contact100-form validate-form">
						@csrf
						<div class="row">
							<div class="col-sm-6 form-group required">
								<label class="bold"> Email ID </label>
								<input name="email" class="form-control" maxlength="255" type="email" value="{{ old('email') }}"  placeholder="Email ID*" id="email" required="required" />
								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
							<div class="col-sm-6 form-group required">
								<label class="bold"> Password </label>
								<input name="password" class="form-control" maxlength="255" type="text" value=""  placeholder="Password*" id="password"  required="required" />
								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
						</div>	
						<div class="submit">
							<input type="submit" name="Submit" value="Login"  id="savedata" class="btn w-100 primary-btn"  > 
							
						</div>
					</form>	
					<p> For further details, you  may contact : <br />
					<strong> Registrations :</strong> <a href="mailto:frames.registration@ficci.com"> frames.registration@ficci.com </a> | 
					<strong> Programme :</strong>  <a href="mailto:amit.tyagi@ficci.com"> amit.tyagi@ficci.com </a> <br />
					<strong> Sponsorship's/E-stalls/Advertisement :</strong>  <a href="mailto:samir.kumar@ficci.com"> samir.kumar@ficci.com </a>
				</div> <!-- Row -->
			</div>
		</div>
	</div>
</section>
	
<style>
#partners p  { margin-top:5px; font-size:14px; line-height:20px; }
#partners .supporter-logo { height:137px; padding:0 10px; }
.col-xs-6 { -ms-flex: 0 0 50%;  flex: 0 0 50%; max-width: 50%; }
</style>
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="  text-center" style="margin-bottom:10px;">
				<h3>Partners</h3>
			</div>
			<div class="row no-gutters supporters-wrap clearfix">
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.startv.com/" target="_blank" class="supporter-logo"> 
						<p> <strong> Convention partner </strong> </p> <img src="{{asset('img/sponsor/star.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>				
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.intel.in/content/www/in/en/homepage.html" target="_blank"  class="supporter-logo"> 
						<p> <strong> Diamond partner & BAF Awards  Co-Presenter </strong></p><img src="{{asset('img/sponsor/intel.jpg')}}" class="img-fluid" alt="">   
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.netflix.com/in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Gold partner </strong> </p> <img src="{{asset('img/sponsor/netflix.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.motionpictures.org/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate partner </strong> </p>  <img src="{{asset('img/sponsor/mpa.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.discovery.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate Partner </strong> </p>  <img src="{{asset('img/sponsor/discovery.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.ufomoviez.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Cine Media Partner </strong> </p> <img src="{{asset('img/sponsor/ufo.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.mediabrief.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p> <img src="{{asset('img/sponsor/media_brief.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.medianews4u.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/media_news.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.adgully.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/adgully.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
					<a href="https://www.mxplayer.in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> OTT Partner </strong> </p>  <img src="{{asset('img/sponsor/mx-player.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
			</div>
		</div>
	</section>  
	
	
@include('frontend.layout.footer')


