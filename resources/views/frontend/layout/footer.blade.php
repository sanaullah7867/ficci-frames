    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-text">
                        <!-- <div class="ft-logo"> <a href="#" class="footer-logo"><img src="img/logo.jpg" alt=""></a> </div> -->
                        <div class="copyright-text"><p>  Copyright &copy; All rights reserved FICCI Frames</a> </p></div>
                        <div class="ft-social">
                            <a href="https://www.facebook.com/ficciindia" target="_blank"><i class="fa fa-facebook"></i> - FicciIndia </a>
                            <a href="https://www.facebook.com/FicciFrames/" target="_blank"><i class="fa fa-facebook"></i> - FicciFrames </a>
                            <a href="https://twitter.com/ficci_india" target="_blank"><i class="fa fa-twitter"></i> - FicciIndia </a>
                            <a href="https://twitter.com/ficciframes" target="_blank"><i class="fa fa-twitter"></i> - FicciFrames </a>
                            <a href="https://www.youtube.com/user/ficciindia" target="_blank"><i class="fa fa-youtube-play"></i> - FicciIndia</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
	<script src="{{asset('js/popper.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/custom.js')}}"></script>
	
</body>
</html>
