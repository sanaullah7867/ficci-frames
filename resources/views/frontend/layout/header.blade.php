<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title> FICCI Frames </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
	
	<link rel="icon" type="image/png" href="img/favicon.ico"/>
    <link rel="apple-touch-icon" href="img/favicon.ico" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
	
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
</head>

<body id="page-top">
   
	<nav class="navbar navbar-expand-lg fixed-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger logo" href="/#page-top"> <img src="img/logo.jpg" class="img-fluid" alt=""/></a>
			<button class="navbar-toggler navbar-toggler-right button_container" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fa fa-bars"></i>
			</button>
			<div class="nav-menu collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#page-top"> Home </a> </li>
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#about"> About </a> </li>
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#speakers"> Speakers </a> </li>
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#agenda"> Agenda  </a> </li>
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#pricing"> Pricing  </a> </li>
					<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="/#partners"> Partners </a> </li>
					<li class="nav-item"> <a class="primary-btn top-btn" href="/registration"> REGISTER</a> </li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="nav_height"></div>

