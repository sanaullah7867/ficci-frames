@include('frontend.layout.header')

<style>
.nav_height { height:86px; }
@media (max-width: 767px) { 
.nav_height { height:57px; }
}
</style>
    <!-- Hero Section Begin -->
    <section class="hero-section set-bg" data-setbg="img/hero.jpg">
        <div class="container">
            <div class="row justify-content-md-center counter-section">                
                <div class="col-lg-12 text-center"> 
					<img src="img/ficci_frames.jpg" class="img-fluid mx-auto d-block ficci_frames" />
                    <!-- <div class="hero-text"> <h1 style="color:#fff;"> We will be live in </h1> </div> -->
					<div class="timer text-center">
						<ul id="future_date" class="text-center"></ul>
						<script>
							$(function(){
								$('#future_date').countdowntimer({
									dateAndTime : "2020/07/07 10:00:00",
									size : "lg",
									regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
									regexpReplaceWith: "<li>$1<sup class='displayformat'>Days</sup></li><li> $2<sup class='displayformat'>Hours</sup></li><li> $3<sup class='displayformat'>Mins</sup></li><li> $4<sup class='displayformat'>Secs</sup></li>"
								});
							});
						</script>
					</div>	
					<br />              
                </div>					
				<div class="col-lg-4 col-md-5 col-sm-4 col-xs-10 text-center">
					<a href="http://www.anica.it/" target="_blank" class="d-block"> 
						<img src="img/sponsor/italy_1.jpg" class="img-fluid" alt="">
						<p style="color:#fff; margin-top:10px;"> Italy - Partner Country </p>
					</a>
				</div> 
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

  

    <!-- Home About Section Begin -->
    <section class="home-about-section spad" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ha-pic">
                        <img src="img/h-about.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ha-text text-justify">
                        <h2>About FICCI E-Frames</h2>
                        <p> E-FRAMES is first & largest five days Global Online Convention covering the entire gamut of Media & Entertainment like Films, Broadcast (TV & Radio), <p> Print Media, Digital Entertainment, Advertisement, Live Entertainment Events, Digital & New Media, Animation, Gaming, Visual Effects etc. </p>
						<p> It will attract not only the industry stakeholders from across the world, but also the government policy makers from India and abroad. </p>
						<p> It will be attended by Indian & Foreign representatives encompassing the entire universe of Media and Entertainment. </p>
						<p> Over the years FRAMES, had honour of being addressed by very well- known and highly successful international and national dignitaries. </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home About Section End -->
	
    <!-- 
    <section class="home-about-section spad" style="padding-top:20px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ha-text text-center">
						<h2>FICCI President</h2>
					</div>
				</div>
				<div class="col-lg-9">
                    <div class="ha-text text-justify">
                        <h3> Uday Shankar <br /> Senior Vice President.</h3>
                        <p> Uday Shankar is the first ever Indian media and entertainment executive to assume the leadership position in a national industry chamber like FICCI, which is also the oldest in the country.</p>
						<p> He is currently Chairman and CEO of Star India and President of 21st Century Fox Asia-Pacific (Only Asia). </p>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ha-pic">
                        <img src="img/speaker/uday_shankar.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
	-->
    <!-- Home About Section End -->
	
    <!-- Team Member Section Begin -->
    <section class="team-member-section" id="speakers">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Speakers</h2>                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="member-item set-bg" data-setbg="img/speaker/uday_shankar.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#uday_shankar">
				<div class="mi-text">
					<h5> Uday Shankar </h5>
					<span> President - The Walt Disney Company Asia Pacific and Chairman – Star & Disney and Senior Vice President - FICCI </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/ajit_pai.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ajit_pai">
				<div class="mi-text">
					<h5> Ajit Pai </h5>
					<span> Chairman <br /> Federal Communications Commission USA </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/mark_read.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#mark_read">
				<div class="mi-text">
					<h5> Mark Read </h5>
					<span> Chief Executive Officer <br /> WPP Group </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/adarsh_nair.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#adarsh_nair">
				<div class="mi-text">
					<h5> Adarsh Nair </h5>
					<span> Chief Product and Experience Officer, Airtel. CEO, Wynk Ltd   </span>
				</div>
            </div>
        </div>
	
		<div class="member-item set-bg" data-setbg="img/speaker/adam_rumanek.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#adam_rumanek">
				<div class="mi-text">
					<h5> Adam Rumanek </h5>
					<span> Media Entrepreneur, Chief Growth Officer <br /> FOUNDER & CEO, AUX MODE   </span>
				</div>
            </div>
        </div>

        <div class="member-item set-bg" data-setbg="img/speaker/ajit_mohan.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ajit_mohan">
				<div class="mi-text">
					<h5> Ajit Mohan </h5>
					<span> Vice President and Managing Director <br /> Facebook India </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/alan.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#alan">
				<div class="mi-text">
					<h5> Alan Milligan </h5>
					<span> CEO-White Rabbit </span>
				</div>
            </div>
        </div>
		 
		<div class="member-item set-bg" data-setbg="img/speaker/alex_fane.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#alex_fane">
				<div class="mi-text">
					<h5> Alex Fane </h5>
					<span> MD of Fane  </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/alok_tandon.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#alok_tandon">
				<div class="mi-text">
					<h5> Alok Tandon </h5>
					<span> Chief Executive Officer <br /> Inox Leisure Ltd </span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/akshat_rathee.JPG">
            <div class="hover d-block" data-toggle="modal" data-target="#akshat_rathee">
				<div class="mi-text">
				<h5> Akshat Rathee  </h5>
					<span> Founder, Managing Director   <br /> NODWIN Gaming  </span>
				</div>
            </div>
        </div>
		
		<div class="member-item set-bg" data-setbg="img/speaker/anuradha_sen_gupta.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#anuradha_sen_gupta">
				<div class="mi-text">
					
					<h5> Anuradha Sen Gupta  </h5>
					
					<span> Consulting Editor  <br /> The Media Dialogues’ on CNBC-TV18  </span>
				</div>
            </div>
        </div>
		

		<div class="member-item set-bg" data-setbg="img/speaker/carl_schmidt.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#carl_schmidt">
				<div class="mi-text">
					<h5> Carl Schmidt  </h5>
					<span>  </span>
				</div>
            </div>
        </div>

		

		<div class="member-item set-bg" data-setbg="img/speaker/carter_pilcher.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#carter_pilcher">
				<div class="mi-text">
					<h5> Carter Pilcher </h5>
					<span> Chief Executive, ShortsTV </span>
				</div>
            </div>
        </div>
		
        <div class="member-item set-bg" data-setbg="img/speaker/colin_burrows.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#colin_burrows">
				<div class="mi-text">
					<h5> Colin Burrows </h5>
					<span> CEO, Special Treats Productions </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/deepak_choudhary.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#deepak_choudhary">
				<div class="mi-text">
					<h5> Deepak Choudhary </h5>
					<span> Founder and Director <br /> Event Capital </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/devang_sampat.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#devang_sampat">
				<div class="mi-text">
					<h5> Devang Sampat </h5>
					<span>Managing Director & CEO <br /> Cinépolis India </span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/devendra.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#devendra">
				<div class="mi-text">
					<h5> Devendra </h5>
					<span>CEO <br /> Spencer’s Retail Limited and Nature’s Basket  </span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/dipti_kotak.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#dipti_kotak">
				<div class="mi-text">
					<h5> Dipti Kotak   </h5>
					<span>Chief Legal Officer  <br /> Reliance Industries Limited’s Media Business </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/fatima_salman.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#fatima_salman">
				<div class="mi-text">
					<h5> Fatima Salman </h5>
					<span> Assistant Director, South Asia Center, Atlantic Council </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/ganesh_rajaram.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ganesh_rajaram">
				<div class="mi-text">
					<h5> Ganesh Rajaram  </h5>
					<span> Executive Vice President Asia <br /> Fremantle International </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/girish_menon.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#girish_menon">
				<div class="mi-text">
					<h5> Girish Menon  </h5>
					<span> Partner and Head, Media and Entertainment, KPMG India </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/harsh_jain.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#harsh_jain">
				<div class="mi-text">
					<h5> Harsh Jain </h5>
					<span> CEO & Co-Founder <br /> Dream Sports (Dream11) </span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/jehil_thakkar.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#jehil_thakkar">
				<div class="mi-text">
					<h5> Jehil Thakkar </h5>
					<span> leads the Media and Entertainment  <br /> Deloitte </span>
				</div>
            </div>
        </div>

        <div class="member-item set-bg" data-setbg="img/speaker/kabir_chandra.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#kabir_chandra">
				<div class="mi-text">
					<h5> Kabir Chandra </h5>
					<span> Music Partnership Lead - India <br /> Facebook India </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/kamal_gianchandani.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#kamal_gianchandani">
				<div class="mi-text">
					<h5> Kamal Gianchandani </h5>
					<span> CEO – PVR Pictures and Chief of Strategy <br /> PVR Ltd </span>
				</div>
            </div>
        </div>
		<div class="member-item set-bg" data-setbg="img/speaker/kapil_agarwal.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#kapil_agarwal">
				<div class="mi-text">
					<h5> Kapil Agarwal </h5>
					<span> Joint Managing Director <br /> UFO Moviez India Ltd. </span>
				</div>
            </div>
        </div>

		
        <div class="member-item set-bg" data-setbg="img/speaker/karan_bedi.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#karan_bedi">
				<div class="mi-text">
					<h5> Karan Bedi </h5>
					<span>Chief Executive Officer <br /> MX Player</span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/madhur_ramchandra_bhandarkar.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#madhur_ramchandra_bhandarkar">
				<div class="mi-text">
					<h5> Madhur Ramchandra Bhandarkar </h5>
					<span>Four Times National Award-winning Indian Film Director, Producer and Script-Writer</span>

					<p>   
				</div>
            </div>
        </div>
		
        <div class="member-item set-bg" data-setbg="img/speaker/manish_agarwal.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#manish_agarwal">
				<div class="mi-text">
					<h5> Manish Agarwal </h5>
					<span>  CEO <br />  Nazara Technologies Ltd. </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/megha_tata.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#megha_tata">
				<div class="mi-text">
					<h5> Megha Tata </h5>
					<span> Managing Director - South Asia <br /> Discovery </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/pranjal_sharma.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#pranjal_sharma">
				<div class="mi-text">
					<h5> Pranjal Sharma  </h5>
					<span> Economic Analyst and Author, India Automated </span>
				</div>
            </div>
        </div>
		<div class="member-item set-bg" data-setbg="img/speaker/priyanka_joshi.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#priyanka_joshi">
				<div class="mi-text">
					<h5> Priyanka Joshi  </h5>
					<span> Intellectual Property Attorney Specialising in Music Related Matters </span>
				</div>
            </div>
        </div>
		
        <div class="member-item set-bg" data-setbg="img/speaker/ritu_kapur.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ritu_kapur">
				<div class="mi-text">
					<h5> Ritu Kapur </h5>
					<span> CO-FOUNDER AND CEO, <br /> QUINTILLION MEDIA (TheQuint) </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/ravi_velhal.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ravi_velhal">
				<div class="mi-text">
					<h5> Ravindra Velhal </h5>
					<span> Global Content Strategist. Intel Corporation </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/roshan_abbas.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#roshan_abbas">
				<div class="mi-text">
					<h5> Roshan Abbas </h5>
					<span> Managing Director <br /> Geometry Encompass </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/shoojit.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#shoojit">
				<div class="mi-text">
					<h5> Shoojit Sircar </h5>
					<span> Film Director and producer </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/sam_carlisle.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#sam_carlisle">
				<div class="mi-text">
					<h5> Sam Carlisle </h5>
					<span> Senior Director, External Partner Relations, Microsoft </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/stephan_ottenbruch.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#stephan_ottenbruch">
				<div class="mi-text">
					<h5> Stephan Ottenbruch </h5>
					<span> Festival Director, Indo German Film Festival </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/sunil_lulla.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#sunil_lulla">
				<div class="mi-text">
					<h5> Sunil Lulla </h5>
					<span> Chief Executive Officer <br /> BARC India </span>
				</div>
            </div>
        </div>
		<div class="member-item set-bg" data-setbg="img/speaker/taejin_lee.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#taejin_lee">
				<div class="mi-text">
					<h5> Taejin Lee </h5>
					<span> Regional Director for the Philippines at Korea Copyright Protection Agency</span>
				</div>
            </div>
        </div>
		<div class="member-item set-bg" data-setbg="img/speaker/taran_adarsh.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#taran_adarsh">
				<div class="mi-text">
					<h5> Taran Adarsh </h5>
					<span> Active Film Critic and Business Analyst since four Decades</span>
				</div>
            </div>
        </div>

        <div class="member-item set-bg" data-setbg="img/speaker/tarsame_mittal.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#tarsame_mittal">
				<div class="mi-text">
					<h5> Tarsame Mittal </h5>
					<span> Founder - TM Talent Management Organisation</span>
				</div>
            </div>
        </div>

		<div class="member-item set-bg" data-setbg="img/speaker/tarun_katial.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#tarun_katial">
				<div class="mi-text">
					<h5> Tarun Katial </h5>
					<span> CEO <br /> ZEE5 India</span>
				</div>
            </div>
        </div>


        <div class="member-item set-bg" data-setbg="img/speaker/ted_schilowitz.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#ted_schilowitz">
				<div class="mi-text">
					<h5> Ted Schilowitz </h5>
					<span> CTO and Futurist, Paramount Pictures </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/trevor_fernandes.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#trevor_fernandes">
				<div class="mi-text">
					<h5> Trevor Fernandes </h5>
					<span> Vice President, Government Affairs Asia Pacific Region, Motion Pictures Association </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/vanita_kohli.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#vanita_kohli">
				<div class="mi-text">
					<h5> Vanita Kohli Khandekar</h5>
					<span> Contributing Editor <br /> Business Standard </span>
				</div>
            </div>
        </div>
		<div class="member-item set-bg" data-setbg="img/speaker/vikash_jaiswal.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#vikash_jaiswal">
				<div class="mi-text">
					<h5> Vikash Jaiswal </h5>
					<span>  CREATOR of All Time Hit mobile game Ludo King. <br /> Founder and CEO of Gametion Technologies </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/vivek_singh.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#vivek_singh">
				<div class="mi-text">
					<h5> Vivek Singh  </h5>
					<span>  Jt. MD <br />  Procam International Pvt Limited </span>
				</div>
            </div>
        </div>
        <div class="member-item set-bg" data-setbg="img/speaker/vynsley_fernandes.jpg">
            <div class="hover d-block" data-toggle="modal" data-target="#vynsley_fernandes">
				<div class="mi-text">
					<h5> Vynsley Fernandes </h5>
					<span>Chief Executive Officer <br /> IndusInd Media & Communications Ltd. </span>
				</div>
            </div>
        </div> 
    </section>
    <!-- Team Member Section End -->

    <!-- Schedule Section Begin -->
    <section class="schedule-section spad" id="agenda">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2> Agenda </h2> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="schedule-tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">
                                    <h5>Day 1</h5>
                                    <p>July 07, 2020</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">
                                    <h5>Day 2</h5>
                                    <p>July 08, 2020</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">
                                    <h5>Day 3</h5>
                                    <p>July 09, 2020</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">
                                    <h5>Day 4</h5>
                                    <p>July 10, 2020</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab">
                                    <h5>Day 5</h5>
                                    <p>July 11, 2020</p>
                                </a>
                            </li>
                        </ul><!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="st-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
												<br />
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<tr>
															<th width="180px"> Time </th>
															<th> Details </th>
														</tr>
														<tr>
															<td width="180px"> <i class="fa fa-clock-o"></i> 11:00 - 12:00 hrs </td>
															<td> 
																<h5><strong>Inaugural </strong> </h5>
																<p class="mb-0"> <strong>Keynote Address: The Role of Ministry of Information & Broadcasting in Unlocking the Value of India's Creative Economy</strong></p>
																


															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 12:00 - 13:00 hrs </td>
															<td> 
															<p><strong>Shaping the Future of M&E in Today’s Digitalised and Information Driven Economy</strong>  </p>
															
															<p> The M&E sector has been radically disrupted over the last decade by the internet and digital media platforms. Consequently, business models have been upended and the entire M&E ecosystem has transformed with the introduction of new business models for content and distribution.  It has also empowered consumers with freedom and choice. This presents both challenges and opportunities for the new digitalised and information driven economy.   </p>
															
															<br />
															
														</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 15:00 - 16:00 hrs </td>
															<td> 
																
																<p class="mb-0"> <strong>Mr. Mark Read,</strong> Chief Executive Officer - WPP group </p>
																<p class="mb-0"> In conversation with <strong><br />Mr. Sudhanshu Vats,</strong> MD & CEO -EsselPropack Limited </p> 
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 16:00 - 18:00 hrs </td>
															<td> <p class="mb-0"> <strong> Session with Partner Country Italy </strong> </p> </td>
														</tr>
													</table>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="st-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
												<br />
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<tr>
															<th width="180px"> Time </th>
															<th> Details </th>
														</tr>
														<tr>
															<td width="180px"> <i class="fa fa-clock-o"></i> 11:00 - 12:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> The Business of Music in an Artist First Economy </strong> <br />

																Music industry has shown consistent growth over the last 3 years and has embraced the digital era. More importantly, the industry is finally out of the shadows of Bollywood/Film music. The non-film / regional music sector has created a pop music industry that reflects the real India. Artists are being discovered from the remotest corners of India and their music is being consumed through multiple touch points. The business is set to skyrocket with the growth and proliferation of digital streaming services, devices, and internet mobile platforms. How can the business of music transcend to the next level to keep up with a constantly changing ecosystem? </p>  <br />

															
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 12:00 - 13:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> The Era of Smart TVs and Interconnected Home Devices Needs Smart Regulations  </strong> 
																<br />
																India’s broadcast sector, one of the largest in the world, now braces for an era of content led by digital and immersive technology platforms for consumer engagement through multiple screens. The lockdown has resulted in a significant decline in subscription and advertising revenues and the adoption of video on demand services has seen a perceptible increase. In this context, regulations and compliances in the linear broadcasting sector must level down to ensure quality content offerings and audience retention to enable the sector to effectively compete and innovate in today’s dynamic and complex marketplace. </p> 

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 15:00 - 16:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Video Games– The Road Beyond the Lockdown </strong> <br />
																Video games saw a huge growth in popularity during the lockdown. Faced with limited options for entertainment during the lock-down, Indians took to video games in a big way. Is video game’s newfound popularity in India a temporary aberration or the new reality?</p>
																<p class="mb-0"> What are the trends in video games consumption during the lock-down and were they different from the earlier normal? As the rules of lock-down ease and other options of entertainment become available, will gaming manage to hold on to its fan (player) base? What can game developers and publishers do to capitalize on the gains made during the lock-down? What will be the new normal in how we make and consume games?</p>

																
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 16:00 - 17:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong>  Attribution at the Forefront of Conversation </strong> <br />
																Measuring the efficacy of advertising spend will become even more critical for companies as they navigate their recovery given that there will be significant pressure of their cash flows and liquidity positions. How can the various M&E segments respond to these enhanced requirements on attribution? How can measurement become timelier and more precise for brands? </p> 

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 17:00 - 18:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Case Study on Fauda with Makers & Cast </strong> </p>   <br />
																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 18:00 - 20:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Session with Partner Country Italy </strong> </p> 
															</td>
														</tr>
													</table>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="st-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
												<br />
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<tr>
															<th width="180px"> Time </th>
															<th> Details </th>
														</tr>
														<tr>
															<td width="180px"> <i class="fa fa-clock-o"></i> 11:00 - 12:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Re-Setting the Sports Ecosystem: Opportunities in “The New Normal World" </strong> <br />
																The sports ecosystem has been ravaged by the impact of the pandemic and its resulting lockdown. Sporting activities have come to a complete halt, significantly impacting all stakeholders, from athletes to recreational participants, equipment makers, team owners, leagues, and federations. As businesses plan to reset and adapt to the new normal, the sporting economy also has the opportunity to restructure and re-build a more sustainable ecosystem, poised for growth. The objective of the session is to discuss government and private participation, sustainable business models and ideas for future growth. </p> 

																
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 12:00 - 13:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Changing Theatrical Windows – Direct to OTT </strong> </p> 
																
																<p>GulaboSitabo was the first Hindi movie to bypass a theatrical release and premiere directly on OTT, much to the consternation of exhibitors who have threatened retaliatory action. While this happened under unforeseen Covid situation all over the world, the pundits have argued that OTT & theatre shall co-exist without OTT being a long term threat to Theatres. How OTT & theatres can grow together in post Covid world?</p> 
																
																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 15:00 - 16:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Mr. Anant Goenka,</strong> Executive Director - Indian Express Group  </p> 

																<p><strong>In Conversation </strong><p>
															</td>
														</tr>

														<tr>
															<td> <i class="fa fa-clock-o"></i> 16:00 - 17:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Virtually Live: can  live events survive in a pandemic world	</strong> <br />
																<p>If people are expected to stay away from outdoor events – concerts, plays etc. – how can the content be brought home? What is the impact on the target audience as events move online? How will this affect content production formats and pricing?  </p> 

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 17:00 - 18:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Accelerated Transformation: Opportunities and Challenges for OTTs in the Crisis Economy </strong> </p> 

																<p>The OTT platforms are taking outlier themes to create not only bold stories and bind India and the world together but their subscriber bases also witnessed a huge surge up to 80% amid coronavirus lockdown.  However, in the mid-term and long-term period they face multiple challenges to sustain this growth trajectory from sustaining new content creation, building talent pool, monetization and, above all, from engaging policy makers in imposing legacy regulations before it realizes its full potential.   </p> 
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 18:00 - 19:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Regulating Creativity: Overcoming Legacy Challenges to Shape the Future of M&E </strong> <br />

																The audio-visual sector in India is one of the 12 Champion Sectors identified by the Government of India because of its employment and growth potential. However, the sector is subjected to multiple onerous regulations and FDI restrictions. As the sector transitions into a “new normal,” are there lessons for Indian policy makers from global best practices that allow free market principles to unlock investments, innovation, employment opportunities and value creation in the sector to make  India, the creative hub of the world.
																</p> 

																

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 19:00 - 20:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Future of Immersive Cinema: Technology meets Storytelling for the Hollywood futuristic production </strong> <br>
																In the era of next-generation immersive cinema, storytelling and technology combine in a data-powered entertainment landscape.  A key milestone in the transformation of Hollywood from “pixel to volumetric voxel” was Intel-Paramount Pictures co-production of Grease, Volumetric song Cinema Experience with more than 5 Trillion pixels captured and processed in 3 minutes to capture everything that light has seen. This masterclass session highlights pioneering transformation, best practices to drive entertainment industry to the next frontier of future of immersive cinema, beyond AR/VR/XR.  </p> 

																

															</td>
														</tr>
													</table>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-4" role="tabpanel">
                                <div class="st-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
												<br />
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<tr>
															<th width="180px"> Time </th>
															<th> Details </th>
														</tr>
														<tr>
															<td width="180px"> <i class="fa fa-clock-o"></i> 11:00 - 12:00 hrs </td>
															<td> <p class="mb-0"> <strong>Ensuring a Fair Marketplace for the Creative Economy in a Digital First Marketplace </strong> </p>
															
															<p>A nation’s economic and competitive strength lies in its ability to harness growth through creative businesses and IP-intensive industries. India’s rank on the Global IP Index may have improved over the years, however implementation of IPR polices, legislative and enforcement frameworks leaves a lot to be desired. How can the country unlock the true value of its M&E Champion Services sector and evolve a modern IPR framework where creative industries can compete in a Digital First marketplace? </p> 

																
															

														</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 12:00 - 13:00 hrs </td>

															<td> 
																<p class="mb-0"> <strong> 1<sup>st</sup> Session on Animation </strong> </p> 
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 15:00 - 16:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> The Economic Impact of the Media & Entertainment Sector:A post-COVID-19 Lens & The Way Forward</strong> </p> 

																</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 16:00 - 17:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Taking Indian Content to Global Markets </strong>  <br />
																Digitalization has been a game changer.  It brought day to day distribution of Indian Films to the cinemas in every corner of the world – same along OTT platforms. But most of the Indian Content is reaching the Indian Diaspora only. <br />
																What could be the Game Changer for Indian Content since the technology is there and distribution is not a hurdle any longer? What are the strategies to create and distribute content to go global. What are World Sales Agents, TV-Stations and platforms abroad looking for? And what are the right “Ingredients” for universal stories from India, which make Indian Content travel around the world?
																</p> 

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 17:00 - 18:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong>  2<sup>nd</sup> Session on Animation </strong> </p> 
															</td>
														</tr>
													</table>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-5" role="tabpanel">
                                <div class="st-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
												<br />
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<tr>
															<th width="180px"> Time </th>
															<th> Details </th>
														</tr>
														<tr>
															<td width="180px"> <i class="fa fa-clock-o"></i> 11:00 - 12:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Living with Covid – 19:The challenges Facing the Cinema Industry </strong> <br />
																Social distancing might continue to be the norm even when lockdowns are lifted completely. How are exhibitors preparing for the new viewing experience? What are the changes and impact to distribution economics by virtue of an anticipated lower occupancy across halls, lack of content and challenges from OTT?  </p> 

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 12:00 - 13:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Gaming – A cog in the digital ecosystem wheel, or a whole wheel in itself? </strong> <br />
																Gaming is fast emerging as an important aspect of ecosystem offerings by major players. Potentially, it can help in customer acquisition and retention by providing users with a sticky use case. How are digital ecosystem players in India, and global players interested in India, looking at gaming though? Just as another temporary possibility or as a standalone business which can, by itself, generate tremendous traction and possibly, profitability in the future? What is the future of gaming as an offering in India, given the new lessons in the post-COVID era?  </p> 


															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 15:00 - 16:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> The Digital Newspaper is Here to Stay </strong> <br />
																During the lockdown, many households across India stopped delivery of newspapers for fear of virus transmission through these surfaces and turned to digital editions for their daily updates. While these editions started off free, they soon went behind a pay wall. Can this be sustained? What are the innovations around digital models for print that can help with monetisation? Are some genres – magazines and editorially reputed news – seeing better traction with subscriptions?</p> 

																

															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 16:00 - 17:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> Valedictory session </strong> </p> 

																
															</td>
														</tr>
														<tr>
															<td> <i class="fa fa-clock-o"></i> 17:00 - 18:00 hrs </td>
															<td> 
																<p class="mb-0"> <strong> BAF Awards </strong> </p> 
															</td>
														</tr>
													</table>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Schedule Section End -->
	
	<!-- Pricing Section Begin -->
    <section class="pricing-section set-bg spad" id="pricing" data-setbg="img/pricing-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Passes Pricing</h2>
                        <p>Get your event Pass plan</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-8">
                    <div class="price-item">
                        <h4>One Day Pass</h4>
                        <div class="pi-price">
                            <h2><span>Rs.</span>1000 <span class="gst">+ GST</span></h2>
                        </div>
                        <ul>
                            <li> One Day Conference Pass</li>
                            <li> Knowledge Sessions  </li>
							<li> B2B </li>
							<li> Business Networking </li>
							<li> Exhibitions </li>
                        </ul>
                        <a href="{{url('/registration')}}" class="price-btn"> BUY NOW <span class="arrow_right"></span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-8">
                    <div class="price-item">
                        <h4>All Day Pass</h4>
                        <div class="pi-price">
                            <h2><span>Rs.</span>3500 <span class="gst">+ GST</span></h2>
                        </div>
                        <ul>
                            <li>All Day Conference Pass</li>
                            <li> Knowledge Sessions  </li>
							<li> B2B </li>
							<li> Business Networking </li>
							<li> Exhibitions </li>
                        </ul>
                        <a href="{{url('/registration')}}" class="price-btn"> BUY NOW <span class="arrow_right"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pricing Section End -->
	
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
		<center>	<div class="col-lg-3 col-md-4 col-xs-6" style="padding-bottom:10px;">
					<a href="https://www.startv.com/" target="_blank" class="supporter-logo" style="border-bottom:none;border-right:none;"> 
						<p> <strong> Convention partner </strong> </p> <img src="img/sponsor/star.jpg" class="img-fluid" alt=""> 
					</a>
				</div> </center>
			<div class="row no-gutters supporters-wrap clearfix">
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.intel.in/content/www/in/en/homepage.html" target="_blank"  class="supporter-logo"> 
						<p> <strong> Diamond partner & BAF Awards  Co-Presenter </strong></p><img src="img/sponsor/intel.jpg" class="img-fluid" alt="">   
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.netflix.com/in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Gold partner </strong> </p> <img src="img/sponsor/netflix.jpg" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.motionpictures.org/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate partner </strong> </p>  <img src="img/sponsor/mpa.jpg" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.discovery.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate Partner </strong> </p>  <img src="img/sponsor/discovery.jpg" class="img-fluid" alt=""> 
					</a>
				</div>
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.ufomoviez.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Cine Media Partner </strong> </p> <img src="img/sponsor/ufo.jpg" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mediabrief.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p> <img src="img/sponsor/media_brief.jpg" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.medianews4u.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="img/sponsor/media_news.jpg" class="img-fluid" alt=""> 
					</a>
				</div>
				

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.adgully.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="img/sponsor/adgully.jpg" class="img-fluid" alt=""> 
					</a>
				</div>

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mxplayer.in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> OTT Partner </strong> </p>  <img src="img/sponsor/mx-player.jpg" class="img-fluid" alt=""> 
					</a>
				</div>

			</div>
		</div>
	</section>

@include('frontend.layout.footer')

<script src="{{asset('js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/jquery.countdownTimer.js')}}"></script>


<!-- Ajit Pai -->
<div class="modal fade" id="uday_shankar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Uday Shankar <br /> 
					<p> President - The Walt Disney Company Asia Pacific and Chairman – Star & Disney and Senior Vice President - FICCI </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
               <p> Uday Shankar is the first ever Indian media and entertainment executive to assume the leadership position in a national industry chamber like FICCI, which is  also the oldest in the country.</p>
				<p> He is currently Chairman and CEO of Star India and President of 21st Century Fox Asia-Pacific (Only Asia). </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ajit Pai -->
<div class="modal fade" id="ajit_pai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Ajit Pai<br />
					<p> Chairman <br /> Federal Communications Commission USA </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Ajit Pai is the Chairman of the Federal Communications Commission. He was designated Chairman by President Donald J. Trump in January 2017. He had previously served as Commissioner at the FCC, appointed by then-President Barack Obama and confirmed unanimously by the United States Senate in May 2012. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ajit Pai -->
<div class="modal fade" id="mark_read" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Mark Read <br />
					<p> Chief Executive Officer <br /> WPP Group </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Mark Read was appointed CEO of WPP in September 2018. Mark has held multiple leadership positions across the company, including nine years as an Executive Director of WPP plc. For 12 years, as Head of Strategy and then CEO of WPP Digital, he was responsible for the company's move into technology through the acquisition of 24/7 Real Media, the creation of data-driven creative network POSSIBLE and the launch of Stream, WPP's celebrated "unconference". </p>
				
				<p> In 2015, he was appointed Global CEO of Wunderman, where he transformed the network into one of the world's leading creative, data and technology agencies. Wunderman joined forces with J. Walter Thompson in November 2018 to become Wunderman Thompson, a global provider of end-to-end solutions and one of WPP's largest businesses, with more than 20,000 people across 90 markets and clients including Microsoft, Dell, Shell, HSBC, BT and Adidas. </p>
				<p> In April 2018 he was named joint Chief Operating Officer of WPP, with responsibility for clients, operating companies and people. </p>
				<p> Earlier in his career, Mark co-founded and developed internet start-up WebRewards. He also specialised in the media and marketing industries as a principal at consultancy Booz Allen & Hamilton, having started his career at WPP working on corporate development. </p>
				<p> Mark was named in the HERoes Champions of Women in Business list (2018 and 2019), which recognises those who have championed women throughout their careers and are supporting women through the talent pipeline. He is also a member of the Male Champions of Change Global Tech Group, which is committed to accelerating gender equality in the technology sector. </p>
				<p> Mark has an MBA from INSEAD and an Economics degree from Trinity College, Cambridge University, and was a Henry Fellow at Harvard University. He is the Chairman of the Natural History Museum Digital Council. Wired magazine ranked him as one of the Top 25 Digital Influencers in Europe in 2014 and he was named The Drum's Digital Individual of the Year in 2015 and 2017. </p>
				<p> He lives in London with his wife and two children. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ajit Mohan -->
<div class="modal fade" id="adarsh_nair" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Adarsh Nair <br /> 
					<p> Chief Product and Experience Officer, Airtel. CEO, Wynk Ltd </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Adarsh Nair is the Chief Product & Experience Officer at Bharti Airtel. He is also the CEO of Wynk and Xstream OTT business. Adarsh's key focus areas include product vision and strategy, and customer experience across Airtel's businesses. He is also responsible for the P&L of Airtel's digital ventures. </p>
				
				<p> Adarsh joined Airtel after spending 16+ years in the United States, where he worked in leadership roles across startups and Fortune 50 tech companies. Before Airtel, Adarsh was the VP of product and growth at Convoy, a $2.7B startup disrupting the global freight marketplace. Adarsh also worked at Marchex, an Advertising tech company, where he led product, data science and engineering to pioneer technologies around omnichannel measurement and attribution. Adarsh also worked in leadership roles at Microsoft and was a strategy consultant at Mckinsey where he had the opportunity to work on video gaming, entertainment, retail, web and OS related businesses. </p>
				
				<p> Adarsh is an active investor in startups across the globe and is also an advisor to the Street Business School, a non-profit focused on uplifting women from poverty through entrepreneurship across the world. </p>
				
				<p> Adarsh has an M.B.A. from the Wharton School. He also completed his M.S. from University of Florida and B.E. from BITS-Pilani. His wife, Archana Nair, works at Microsoft and they are blessed with two boys. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Adam Rumanek -->
<div class="modal fade" id="adam_rumanek" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Adam Rumanek <br /> 
					<p> Media Entrepreneur, Chief Growth Officer FOUNDER & CEO, AUX MODE    </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Entrepreneur. Author. Internet programmer. Since selling his first Internet company in the year 2000, Adam has created industrylauded programs and companies in web, content, big data and nextgeneration technology. For over a decade, Adam has collaborated globally with corporations, governments, and NGOs, helping enable them to maximize the business potential of digital media platforms. </p>
				
				<p> In 2013, the eruption of YouTube – and the exponential population explosion of YouTubers – alerted Adam to the need for a Multi Channel Network (MCN) to help manage and protect users. His solution, and his next start-up venture, he named Aux Mode. 
It now appeals to the massive community of YouTube creators that today consist of over onethird of the Internet’s users. </p>
				
				<p> Within four years of its inception, Aux Mode gained 1750+ clients who are responsible for the generation of 2+ billion views permonth, in a staggering variety of genres. His boutique tech company is continually being recognized by industry leaders, most recently listed on The Globe and Mail’s Top 3 most promising startups in Canada for 2017, certifying that Aux Mode is indeed positioned well ahead of the curve.  </p>
				
				<p> Adam’s unquenchable passion to collaborate with content creators, studios, and brands 
across the YouTube platform has accelerated Aux Mode’s unique offerings. A strategic focus on building the brands of both studios and content creators, strongly positions Aux Mode to continuously increase views and shareability, all while providing content optimization and protecting intellectual property </p>

<p>As a speaker, Adam is a dynamic storyteller, weaving anecdotes through extensive digital knowledge. He generously supplies his audiences with practical advice on protection, optimization, and monetization of content within a disruptive media ecosystem. He frequently speaks at technology & creative venues like Google, Soho House and YouTube.  </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Priyanka Joshi --> 
<div class="modal fade" id="priyanka_joshi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Priyanka Joshi <br /> 
					<p> Intellectual Property Attorney Specialising in Music Related Matters.    </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>She has previously worked at Times Music and Zee Music Company and is currently the General Secretary at The Indian Music Industry where she is responsible for driving legal strategy and public policy in connection with the recorded music industry. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Ajit Mohan -->
<div class="modal fade" id="ajit_mohan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Ajit Mohan <br />
					<p> Vice President and Managing Director <br /> Facebook </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> As the Vice President and Managing Director, India at Facebook, Ajit Mohan spearheads the company's India charter across the family of apps. Prior to Facebook, Ajit was the Founding CEO at Hotstar. He launched and built Hotstar into India's leading premium video streaming platform. Ajit spent five years at McKinsey & Company, first in New York in its media practice and two years as a Fellow at the McKinsey Global Institute. He is a graduate of Johns Hopkins University and the Wharton School, and he holds a BASc in Computer Engineering from NTU, Singapore. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ajit Mohan -->
<div class="modal fade" id="alan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Alan <br />
					<p> Ceo white rabbit  </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Alan's films have won awards in Cannes (Un Certain Regard) and Venice (Critics) with nominations in Berlin, Toronto and Busan. As a producer, co-producer and executive producer, the films have won more than 50 awards and nominations since 2014. </p>

				<p> Alan was formerly a software entrepreneur with MCIS, a database for safe transportation of chemicals at sea. MCIS became the industry safety standard for the $30Bn chemical tanker market. MCIS was sold to Heidenreich Innovation in 2008. </p>

				<p> With White Rabbit Alan combines his two careers, offering platform independent digital distribution with peer-to-peer and blockchain technology. Fans are able to watch content anywhere, as our browser extension identifies content fans stream and enable them to pay rights holders directly. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- alex_fane -->
<div class="modal fade" id="alex_fane" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Alex Fane  <br />
					<p> MD of Fane   </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Alex Fane – MD of Fane - one of the UK’s fastest growing and most dynamic production companies, working with inspiring voices from the world of literature, podcasts, screen and stage. </p>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Alok Tandon -->
<div class="modal fade" id="alok_tandon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Alok Tandon <br />
					<p> Chief Executive Officer <br /> Inox Leisure Ltd </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Alok Tandon is the Chief Executive Officer (CEO) of INOX Leisure Limited, the 2nd largest multiplex chain in India. As a member of the start-up team of INOX, he has spearheaded the organization for two decades from scratch to a footprint of close to 600 screens across India. </p>
				<p> He has been responsible for making INOX Leisure achieve an aggressive positioning in the market share with a continued business growth. </p>
				<p> In 2016, he was rated among the Business Today-PwC list of India's top 100 CEOs 2016. In 2018, he received the award for CEO Of The Year at the Economic Times Retail Excellence Awards. In June 2019, Business World put him in the coveted list of 20 Most Valuable CEOs in India. As a torch-bearer of the multiplex industry in the retail domain, Alok was recognized as one of India's Retail Icons by the prominent retail magazine, IMAGES Retail in August 2019. India Retail Forum, one of the most celebrated forums of the Retail industry, recognized Alok as 'The Most Admired Retail Personality Of The Year' for FY19.  </p>
				<p> It was in the year 2001, Alok joined INOX Leisure Ltd as Vice President-Technical. From the time when the company was in a start-up mode till date, Alok has successfully steered the growth momentum and built the ethos of the organization around the three pillars of 'Luxury', 'Technology' and 'Service' and strengthening its motto of 'LIVE theMOVI'. </p>
				<p> Alok has over three decades of experience across Entertainment, Hospitality and Pharmaceutical industries and has been instrumental in the set-up and growth across genres.Alok is a speaker at various National & International conferences and symposiums and brings with him vast experience of strategy, growth and innovation. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!--Akshat Rathee -->
<div class="modal fade" id="akshat_rathee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Akshat Rathee  <br />
					<p> Founder, Managing Director  <br /> NODWIN Gaming  </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Akshat Rathee is a serial-entrepreneur and the Managing Director of NODWIN Gaming, a venture by the NODWIN Group of Companies. NODWIN Gaming is the premier and most recognized esports company in India. It has played a pivotal role in developing the Indian esports scene in the country by playing an instrumental role in popularizing competitive gaming through its exclusive license partnerships with ESL and DreamHack. NODWIN Gaming works actively to run other marquee esports IPs such as the Mountain Dew Arena, The CII India Gaming Show among others. NODWIN Gaming additionally produces online content on gaming and esports such as Esports Mania on MTV, a collection of four premium esports shows, The Cosplay Genie Show and Live content from pop culture events in India as well as abroad.  </p>
			
			<p> An entrepreneur at heart, Akshat is also the Managing Director of the NODWIN Group of Companies, core holding group. Housing an assortment of ventures, the NODWIN Group began its journey in 2005 with the objective of providing clients with rarefied and resilient solutions, helping them unravel their businesses. Under Akshat’s guidance, the NODWIN Group has worked with 36 of the Top-100 companies of India in its portfolio and continues to make remarkable strides in the fields of ideation, consultancy, investment, merchandising, retail, video games, esports, media production, real estate, hospitality and much more. Some of the notable ventures under the NODWIN Group banner are the Earth-100 Biofuels, which focuses on clean energy and ensuring a better tomorrow. The NODWIN Consultants, an investment and financial advisory body focused on providing value-based consultancy services.  </p>
				
			<p>Before beginning his journey as an entrepreneur, Akshat worked as a  Senior Consultant with Ernst & Young. Ernst & Young is a global leader in assurance, tax, transaction and advisory services. He also served as an Assistant Manager with IBM Daksh (now IBM Global Process Services). At IBM Daksh, Akshat was involved with business process outsourcing, catering to industries such as telecommunications, retail, airlines, banking, insurance, healthcare, automotive and electronics.  </p>

			<p>Understanding his responsibility towards society, Akshat has co-founded Swabhimaanya, a firm that strives to empower the underprivileged and provide essential services in the form of finance and household items. Akshat is also a prominent member of AIESEC and plays an influential role as an advisor on matters of strategy, leadership and entrepreneurship.  </p>

			<p>Akshat graduated from the Manipal Institute of Technology with a Bachelors in Engineering (Computers). He also holds an MBA in International Business from Ecole des Ponts, France. He boasts of over 18 years of experience in the fields of entrepreneurship, business consultancy and strategy.  </p>

			<p>A gamer since his childhood days, Akshat has turned his passion into a successful business endeavour. Through NODWIN Gaming, Akshat has helped identify the esports potential present in India. He continues to play an influential role in actively shaping the future of competitive gaming in the country.  </p>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- anuradha_sen_gupta-->
<div class="modal fade" id="anuradha_sen_gupta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Anuradha Sen Gupta  <br />
					<p> Consulting Editor <br /> The Media Dialogues’ on CNBC-TV18 </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Anuradha SenGupta is a broadcast journalist working as Consulting Editor with Network18. She is the host and editor of ‘The Media Dialogues’ on CNBC-TV18  a deep dive into the world of brands..
A thoughtful and visual approach to news television and a broad diverse canvas of subjects have been the hallmarks of her long career. Her most recent programs were the docu-news features weekly Reporters Project on CNN-NEWS18 and a free flowing conversation series Life Etc... on CNBC-TV18.  ‘Storyboard edited by Anuradha SenGupta’ was the first show for marketing and brands on Indian television and was a an industry benchmark. Her interview shows ‘Being..’ on CNN-IBN,  ‘Beautiful People’ on CNBC-TV18 and ‘Off Centre’  on CNN-NEWS18 have enjoyed a cult following.
 </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="carl_schmidt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Carl Schmidt  <br />
					<p> Jack Of All Trades </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Carl got his start in games as a "jack-of-all-trades" artist in 1994. In his career, he has worked from a buyer and a service provider perspective, most notably at Electronic Arts, Michael Crichton's Timeline Entertainment, Nvidia and his current role as Senior Director of External Development at Zynga. At Zynga, he has built a worldwide organization which facilitates all external development and partner relations for Art, Engineering, Audio, UI/UX, Co-Dev, QA, Localization/LQA, Culturalization, Narrative, Marketing, User Acquisition and the publishing organization. Most recently, he is also part of Zynga’s acquisition integration team.
 </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Carter Pilcher -->
<div class="modal fade" id="carter_pilcher" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Carter Pilcher <br />
					<p> Chief Executive, ShortsTV </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Carter Pilcher pioneered the world’s leading short movie entertainment company, ShortsTV.</p>
				
				<p>ShortsTV is the first and only 24/7 HD TV channel dedicated to short movies and is available in over100 million households across the US, Mexico, Latin America, Europe and India. The accompanying ShortsTV app, which gives audiences the power to create their own movie channels based on their favourite genres and using a similar machine-learning algorithm to Spotify, has recently launched in the US on DirecTV and in the Netherlands on Ziggo, with plans to roll it out to other countries in 2020. </p>
				
				<p>Online, ShortsTV offers hundreds of the world’s best independent shorts for download in 92 iTunes stores across the globe as well as on Amazon, Google Play, Verizon and Frontier. </p>
				
				<p> Since 2006, ShortsTV has produced the annual theatrical release of The Oscar Nominated Short Films, distributing it to movie theatres around the world and digital sell-through platforms such as iTunes, Amazon and Google Play. The theatrical release of the Oscar Nominated Shortsis now a firm fixture in US & Canadian theatres and in 2019 grossed over $3.5 million at the US box office.  </p>
				
				<p> ShortsTV is operated by Shorts International Ltd, which is headquartered in London and has offices in Los Angeles, Amsterdam and Mumbai. Shorts International has the world’s largest catalogue of short movies with over 13,000 titles across all genres. Catalogue titles include past Oscar winners and nominees, festival favourites and revered classics. </p>

				<p>Carter is a voting member of both BAFTA and the Short Film and Feature Animation Branch of the Academy of Motion Pictures, Arts and Sciences (AMPAS). Before launching Shorts International in London in 2000, he worked in Eastern European/Middle Eastern market origination and corporate finance for Bankers Trust, a leading US investment bank that followed his work as the legislative director and chief counsel for U.S. Senator Hank Brown.</p>
				
				<p>Carter, originally from Terre Haute, Indiana, received a B.S. from the U.S. Air Force Academy, a J.D. from Georgetown University, is a member of the New York Bar and attended the London Business School Corporate Finance Evening Program.</p>
				
				
				<p> <b>ShortsTV in India</b>  </p>
				<ul style="padding-left:15px;">
					<li>  ShortsTV is an ‘added value service’ in India, meaning TV operator customers need to subscribe to the channel for a monthly fee. </li> 
					<li>  The TV service launched in India in November 2018 with Tata Sky.  </li> 
					<li>  We expanded distribution of the TV service to the Airtel Digital TV and Dish TV/d2h platforms in September 2019.  </li> 
					<li>  Our distribution agreements mean ShortsTV is now available to over 65 million Indian households (85% of the country’s direct-to-home satellite TV homes). </li> 
					<li> We also released the Oscar Nominated Short Films in Indian cinemas for the first time in February 2019, with PVR Cinemas. They were screened in cinemas across 9 cities.  </li> 
					<li> We founded the ‘Best of India Short Film Festival’, which aims to qualify more Indian short films for Oscar consideration.</li>
							

				</ul> 
				<div style="padding-left:25px;">
							<p> o	5 finalists are screened in a cinema in Los Angeles for one week (one of the Oscar qualification criteria), with one overall winner selected. </p>
							<p>o	2019 was our second edition of the festival and we received over 3,000 entries</p>
							<p>o	The winner was Half Full, starring veteran Indian actor NaseeruddinShah. </p>
							<p>o	Currently there are no Indian film festivals that are accredited by the Academy for Oscar consideration, which means Indian filmmakers either have to win an award at an accredited film festival overseas or screen their film in a cinema in Los Angeles. In addition to our own film festival, we are working with established local festivals, such as the Bengaluru International Short Film Festival, to get them accredited. </p>
							</div>

				<p><b>The rise of short form entertainment</b></p>
				<p> Short film has always been a part of the filmmaking industry. In fact, in the beginning, there was only short film! In 1878 the first motion was created from photographs and was 16 frames long. In 1888 the first movie was shot and lasted just 2 seconds. In 1932, Laurel and Hardy won the first ever Oscar for a short film. Since then, short film has come in to its own with captivating stories and high production values all helping to attract more mainstream audiences. With three award categories at the Oscars (Live Action, Animation and Documentary), short films have earned the right to stand alongside feature films and people are taking notice. For example, ShortsTV’s annual theatrical release of the Oscar Nominated Short Films has grown in the box office every year since we started putting them in theaters in 2006 and are now screened in over 600 theaters around the world. Celebrity presence in the short film arena has also raised awareness as directors, actors and writers use the medium as a way to try new things or as a launchpad for larger projects, often resulting in very exciting films. British stars to have made short films include Dame Judy Dench, Olivia Colman, Colin Firth, Michael Fassbender, Kiera Knightly, Martin Freeman, among many others. And of course the advent of smartphones has helped viewers discover a love for short form entertainment because short films are perfectly suited to mobile viewing. </p>

				
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Colin Burrows -->
<div class="modal fade" id="colin_burrows" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Colin Burrows <br />
					<p> CEO, Special Treats Productions </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Colin Burrows is an independent producer, publicity consultant and CEO of the Special Treats Production Company, Europe's leading production house for audio-visual coverage of the film industry. Clients include all the Hollywood majors, the Bond films for over 25 years, the HARRY POTTER films, LORD OF THE RINGS/THE HOBBIT etc. </p>
				
				<p> The company also produces award-winning documentaries (TRUTH & LIES: The UB40 story, BBC. THIS IS POP, Sky Television etc) and have worked extensively with corporate companies including Aston Martin, Rolls Royce, DHL etc. </p>
				
				<p> London based, Colin works across the international film spectrum with a special interest in India. He is an international adviser to Federation of India's Chamber of Commerce and Industry's Entertainment division and has executive produced six films in India. </p>
				
				<p> He is co-founder, with Michael Ward, of Beautiful Bay Entertainment. Their first film as producers, BOMBAIRIYA was released by Sony Pictures India in January 2019 and is currently screening on Netflix. </p>
				
				<p> They will remake THE FAR PAVILIONS in 2021 as a major TV series in India and he is also developing a large budget British film, R101, with Academy Award winning producer David Parfitt of Trademark Films </p>
				
				
				<p> His other production credits include a long-standing relationship with Bengali director Suman Ghosh:  </p>
				<ul style="padding-left:15px;">
					<li>  Supervising Producer: NOBEL THIEF, (Suman Ghosh), 2011 London Film Festival, Critics prize at the Bengaluru International Film Festival. Released Feb 2012 </li> 
					<li>  Executive Producer: SHYAMAL UNCLE TURNS OFF THE LIGHTS (Suman Ghosh) Busan International Film Festival and Mumbai Film Festival 2012, US release 2013 </li> 
					<li>  Executive Producer: PEACE HAVEN (Suman Ghosh) 2015 </li> 
					<li>  Producer: THE POACHER (Suman Ghosh) – in development </li> 
				</ul> 
				<p> Colin is a long-standing member of the British Film and Television Academy and supporter of the UK Film & Television Charity. He is on the advisory board of the proposed Havering Studios development and also of the Cambridge based, Storylab academic project. He is a mentor to students at St Johns College, Cambridge, interested in business. </p>

				<p> He is the director of the Turks and Caicos International Film Festival which he created to celebrate ocean and environmental film-making and which held its inaugural event in November 2019. www.TCIFF.org and an adviser to the government there on their plans to create a local film industry. </p>

				<p> He began his broadcasting career at Cambridge University where he was the Programme Controller of Cambridge University Radio (now CamFM). He started as a producer at Capital Radio in the news and documentaries department then moved to Radio One as a producer. He founded Special Treats Productions in 1987 while continuing to work for the BBC. The company produced series for Sky Television, Film Four and broadcasters worldwide and now includes Apple, Netflix and Amazon amongst its clients </p>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Deepak Choudhary-->
<div class="modal fade" id="deepak_choudhary" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Deepak Choudhary <br />
					<p> Founder and Director <br /> Event Capital </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> With over 15 years of experience in the events industry, Mr. Deepak Choudhary founded Event Capital in 2013 as an aggregator of event IPs. He has carved a niche for himself with his multiple entrepreneurial ventures in the events space - from education institutes, to an author along with creation of event IP's. The company has grown remarkably ever since to become India's most diverse live event IP company involved in aggregating and consolidating event IP's across target groups and platforms. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Devang Sampat -->
<div class="modal fade" id="devang_sampat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Devang Sampat <br />
					<p> CEO <br /> Cinepolis India </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Devang Sampat is CEO at Cinepolis India & has been with the company since its inception, 11 years ago. In his leadership role, Devang focuses on Indianizing Cinepolis's international strategies &driving development plans for the Indian environment. He has been an integral part of the team, responsible for Cinepolis's success by leading the national expansion &consistently serving the best customer experience through new & innovative offerings to consumers. </p>
				<p> Devang has been instrumental in growing the multiplex segment in India in the last 20 years & understands the growing demands of Indian consumers& impact on the retail industry like no other. </p>
				<p> Devang holds an MBA degree from ITM Mumbai & is a graduate of Mithibai college, Mumbai University. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Devendra -->
<div class="modal fade" id="devendra" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Devendra  <br />
					<p> Managing Director & CEO  <br /> Spencer’s Retail Limited and Nature’s Basket Ltd</p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>DC is a gold medallist engineer, University rank holder, MBA of SIBM Pune and an alumnus of Harvard Business School via its AMP program.
 </p>
 <p>An enthusiast of consumer behaviour and digital transformation, he recently finished his PG diploma in digital business. Over the years, DC has led leadership positions in Various Indian business houses and global MNCs and worked in several emerging and matured markets. He has served in Asian Paints, Coca Cola, Future Group, Walmart and now is Managing Director & CEO of Spencer’s Retail Limited and Nature’s Basket Ltd.
 </p>
 <p>His thoughts have been published in leading publications like Economic Times, Business Today, Business Standard, Hindustan Times to name a few.
A start-up enthusiast, he has been a mentor to many start-ups; DC takes keen interest in the development of start-up ecosystem. He enjoys sharing the learning’s also and teaches at Management Institutes including Economic Times master classes and sessions on digital transformation with Dr. Ramcharan. He is a Co Chair of FICCI and has served on many committees of CII & FICCI. 
 </p>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- dipti_kotak -->
<div class="modal fade" id="dipti_kotak" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Dipti Kotak <br />
					<p> Chief Legal Officer  <br /> Reliance Industries Limited’s Media Business </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Dipti Kotak  is the Chief Legal Officer at Reliance Industries Limited’s Media Business. As CLO, Dipti leads the Legal & Regulatory framework for JIO Studios which includes RIL Productions, oversee Legal framework for RIL’s other media investments.  As CLO, Dipti is responsible to work closely with business on strategic partnerships, safeguard the business and  facilitate business through proactive law suits, and ability to monetize through contracts, etc.</p>
				<p> Dipti is a domain expert and  has over 25 years of experience in media and entertainment industry, </p>
				<p> Intellectual Property, Corporate and M&A’s, Compliance & Regulatory and Litigation.  </p>
				<p> Prior to Reliance, Dipti worked with Sony Pictures Networks India Private Limited (SPN) since 1996. As Head Legal at SPN, Dipti  partnered with SPN’s various businesses  i.e. Hindi Cluster of channels, Regional and Animation channels, SPN Productions, Music, Digital, Syndication & Licensing,  and handled Industry Matters, Litigation and  Training & Compliances. She was also the Presiding Officer of the Internal Complaints Committee under ASHP. </p>
				<p>Dipti was listed as one of India’s top 100 Powerful Women in Law in 2017 by World IP Forum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Fatima Salman -->
<div class="modal fade" id="fatima_salman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Fatima Salman <br />
					<p> Assistant Director, South Asia Center, Atlantic Council </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Fatima is an Assistant Director at the Atlantic Council's South Asia Center, where she contributes to the Center's policy research initiatives and manages programmatic tasks. Drawing on her experience working in India, Fatima brings a deep understanding of the region based on both her professional and personal experiences in the country. Fatima holds a Master of Science in Foreign Service with a focus in global politics and security from Georgetown University's School of Foreign Service. She holds a Bachelor of Arts in Political Science and History and a Minor in Middle East/South Asia Studies from the University of California, Davis.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ganesh Rajaram -->
<div class="modal fade" id="ganesh_rajaram" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Ganesh Rajaram  <br />
					<p> Executive Vice President Asia <br /> Fremantle International </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Ganesh Rajaram is currently General Manager, Executive Vice President Asia at Fremantle International (FMI). Based in Singapore, Ganesh oversees the Asian distribution, format licensing business and the digital, ancillary business of Fremantle. He oversees a catalogue of more than 20,000 hours of programming including blockbuster hits like the Got Talent franchise, the Idol franchise, Project Runway and the Jamie Oliver franchise, as well as hit global dramas like 'My Brilliant Friend', 'American Gods' and 'The Young Pope franchise.</p>
				<p> An experienced and recognized leader, Ganesh was also a Nominated Member of Parliament in the Singapore Parliament from 2016 to 2018. He was also a Board Member of the Media Development Authority of Singapore from 2010 to 2016. </p>
				<p> Prior to joining Fremantle in 2005, Ganesh had more than 12 years of experience in a fast paced, revenue oriented, competitive media environment from companies such as SPH Mediaworks, Singapore (Vice-President, Programming), where he was instrumental in setting up and running the free-to-air English network Channel i, Mediacorp Singapore (Executive Producer) and Singapore Press Holdings (award-winning sports columnist and Journalist).</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ganesh Rajaram -->
<div class="modal fade" id="girish_menon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Girish Menon <br />
					<p> Partner and Head, Media and Entertainment, KPMG India </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Girish has specialized in the Media and Entertainment sector where he has significant experience having advised clients across most media sub sectors covering both National and Regional players. </p>
				<p> Girish has more than 15 years of advisory and M&A experience and has advised on more than 150 M&A transactions. Girish has extensive experience on buy-side and sell-side assisting clients in evaluating the risks and opportunities of their intended transactions. He has led a number of high profile and complex transactions and thus has a deep understanding and practical experience of dealing with issues that arise on Indian transactions </p>
				<p> Girish has also had a stint where he briefly ran a startup media venture and also worked with Balaji Telefilms as Head –Strategy and New Ventures. </p>
				<p> Girish brings on board an unique blend of strategic thinking and financial understanding combined with a solid operational understanding of businesses. Working closely with various media companies, he has developed a strong understanding and insight into the dynamics of various media sectors, related issues and the underlying macro and micro drivers and trends </p>
				<p> He leads the KPMG India's annual publication on Media and Entertainment industry </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ganesh Rajaram -->
<div class="modal fade" id="harsh_jain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Harsh Jain  <br />
					<p> CEO & Co-Founder <br /> Dream Sports (Dream11)</p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Harsh Jain is the CEO and Co-Founder of Dream Sports, India's leading sports technology company with brands such as Dream11, FanCode, DreamX, DreamSetGo and DreamPay in its portfolio. </p>
				
				<p> Harsh Jain, along with BhavitSheth, launched Dream11 as India's first freemium fantasy sports platform in 2008. Harsh completed his engineering from the University of Pennsylvania and his MBA from Columbia Business School. Bringing together his love for sports, gaming and technology, Harsh is always working to ‘Make Sports Better' for every sports fan. He has also been instrumental in founding and promoting India's first and only self-regulatory industry body, the Federation of Indian Fantasy Sports (FIFS). </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Ganesh Rajaram -->
<div class="modal fade" id="jehil_thakkar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Jehil Thakkar  <br />
					<p> leads the Media and Entertainment Sector  <br /> Deloitte</p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Jehil leads the Media and Entertainment sector for Deloitte in India. He has over 25 years of experience in the media and entertainment space, and has worked with clients across the US, UK, France, South East Asia, Middle East and Japan, in addition to India. He has worked on strategy, operations business planning and due diligence engagements for film, television, publishing, cable and satellite, music, digital media, and advertising companies. </p>
				
				<p> Prior to joining Deloitte India, Jehil was the sector lead for Media and Entertainment at KPMG in India. Jehil has spent several years in the US where he worked with a number of clients in the M&E space in Los Angeles and New York.</p>

				<p>Jehil was part of a digital media incubator in Los Angeles where he worked with start-ups and entrepreneurial growth companies on business planning, financing, marketing, and operations. Jehil also briefly worked in International Distribution at 20th Century Fox, and in strategic planning at Dreamworks SKG. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Kabir Chandra -->
<div class="modal fade" id="kabir_chandra" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Kabir Chandra <br />
					<p>  Music Partnership Lead - India <br /> Facebook India </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Kabir leads Music Partnerships for Facebook India and was responsible for launching music stickers on both Facebook & Instagram. Yes, he's the reason your friends used the song Baby Shark on all their Instagram stories last year. Kabir has been working in the Indian music industry for the last 15 years and was previously heading the digital department at Tips Music before which he was at Sony Music India. He started his career as an RJ at RadioOne Delhi and launched the city's first college radio show. </p> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Kamal Gianchandani  -->
<div class="modal fade" id="kamal_gianchandani" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Kamal Gianchandani <br />
					<p> CEO – PVR Pictures and Chief of Strategy <br /> PVR Ltd </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Kamal Gianchandani has held a number of senior management positions across the media and entertainment arena and has been the principal architect of company's strategy and drives external relations, film supply, digital & IT operations, filmed entertainment initiatives for the company. He joined the PVR Group in 1995 and since then has many laurels to his name. Various media publications have acknowledged him as one of the most influential filmed entertainment professional in the country, on multiple occasions. He is the recipient of the prestigious 'Best Distributor Stardust Award' in 2010 & 2015. </p>	
				<p> Under his leadership, PVR Pictures Limited has piloted some of the biggest independent films, making it the largest and the most successful distributor of independent foreign language films in India, while continuing to have a strong foot hold in distribution of locally produced films. He spearheaded the acquisition of Cinemax Chain and SPI Cinemas. He holds a Bachelor's degree in Commerce (Hons) from Delhi University, an MBA from Institute of Management Education (IME) Pune, and is an alumnus of Indian School of Business (PGPMAX). Gianchandani comes with a diverse experience of more than 24 years in film financing, co-production, distribution, syndication, licensing, cinema exhibition, on-line streaming, film rental services and general management, his skill set has been instrumental in driving the strategy and business planning at PVR Cinemas. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Kapil Agarwal -->
<div class="modal fade" id="kapil_agarwal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Kapil Agarwal <br />
					<p>  Joint Managing Director <br /> UFO Moviez India Ltd. </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Mr. Kapil Agarwal is the Joint Managing Director of UFO Moviez. His expertise in operations, investment, and finance placed UFO Moviez in good stead during its initial fund-raising stage. It is largely due to his operational and process disciplines that UFO has now acquired market leadership position in the digital cinema space, transforming entertainment and bringing cinema closer to audiences around the world. </p> 

				<p> Mr. Agarwal has over 30 years of experience in varied business environments and has worked with leading industrial houses like the Modi Group, Apollo Tyres Group and British Gas.  He was awarded the CA Business Leader - SME Sector, for exceptional performance and achievement by the Institute of Chartered Accountants of India. </p> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Karan Bedi -->
<div class="modal fade" id="karan_bedi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Karan Bedi <br />
					<p> Chief Executive Officer <br /> MX Player </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> With over a decade of rich experience across media, technology and education industries - Karan Bedi is an alumnus of the prestigious Stanford University. It was there that Karan discovered his passion for entrepreneurial ventures at the intersection of media and technology, especially focused on mobile. </p>
				<p> In his current role as the CEO of MX Player, he is responsible for setting strategy and leading teams across all functions including content, tech, product development, sales, marketing and other business verticals - taking this young OTT platform to the next level. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Madhur Ramchandra Bhandarkar  -->
<div class="modal fade" id="madhur_ramchandra_bhandarkar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
				Madhur Ramchandra Bhandarkar  <br />
					<p>  Four Times National Award-winning Indian Film Director, Producer and Script-Writer  </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Madhur Ramchandra Bhandarkar (Born on 26th August 1966) is a four times National Award-winning Indian film director, producer and script-writer best known for making hard-hitting realistic cinema that not just has the critical acclaim but also has the best commercial success rate in the country. He is best known for his films Chandni Bar (2001), Page 3 (2005), Traffic Signal (2007), Fashion (2008), Jail (2010), Dil Toh Baccha Hai Ji (2011), Heroine (2012) and Indu Sarkar (2017). He has been conferred with Padma Shree, fourth highest civilian award from the Govt. of India in 2016. </p> 

				<p>Bhandarkar's fascination for cinema began when he was a kid. He wanted to be a part of them in some way or the other. At the age of 16, Bhandarkar ran a video cassette library in Khar (W), a suburb of Mumbai, India, and delivered cassettes door-to-door. This gave him access to a large collection of movies and he studied film-making through it.  </p> 

				<p>In late 90's after trying his hand at production and various other stints including as assistant to different film directors, Bhandarkar landed up as an assistant to Indian filmmaker Ram Gopal Varma. He assisted Varma in three movies and even played a small role in his 1995 film Rangeela. A couple of years later he made his directorial debut with the movie Trishakti (1999), an out-and-out popcorn entertainer. However, Bhandarkar wanted to make cinema that he wanted to see and that resulted in a script called Chandani Bar. He went in to rope actress Tabu and actor Atul Kulkarni and made the film that won a National Award for best film on social issues and his lead actress Tabu. The film was critically applauded and a major box-office success, which took Bhandarkar into the top league of filmmakers in Indian film industry. After his first National Award for Chandani Bar, there was no looking back for him.  </p> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Manish Agarwal -->
<div class="modal fade" id="manish_agarwal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Manish Agarwal <br />
					<p> CEO <br />  Nazara Technologies Ltd. </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Manish Agarwal (40), CEO at Nazara Games has over 18 years of industry experience, most of it dedicated to the digital industry. In view of his rich expertise in the digital space that ranges across consumer marketing, product development, developer evangelism and digital monetization, Manish was handed over the responsibility of heading the gaming venture in 2015. </p>
				
				<p> At Nazara, Manish leads a young team of professionals. In the decade since the digital buzz, Manish has guided his teams towards adopting business models that can accommodate the changes of the recurrently shifting digital trends by keeping the monetization arrangement unharmed. ‘Mobile First' being Manish's mantra, the company today concentrates primarily on bringing entertaining and engaging mobile content, catering to audiences the world over.    
				 
				<p> A self-confessed voracious reader, Manish's strength is his ability to completely get in sync with any current digital trends and mould it towards the betterment of his business. He is passionate about all things related to technology and gadgets with a deep interest in mobile gaming. "I want to see India on the world map of mobile gaming and it satisfies me that my company is playing a significant role" he says. Manish today is living his dream to see India on the world map of mobile gaming by creating immensely successful mobile games for global audience.
				 
				<p> Prior to Nazara Games, Manish led various gaming companies like Zapak and Reliance Games etc. Manish has been associated with some leading corporations in the technology and media sector such as Microsoft, Rediff, Infosys and UTV. At Rediff, Manish put forth his experience and expertise of new media businesses and his passion for product innovation and development thereby leading some great projects. He later spent a year at Microsoft as the Director of Marketing and with a rich consumer understanding of the industry, he moved to UTV taking over the responsibility of CEO – New Media Venture Ltd.
				 
				<p> He holds a Bachelor's Degree from the National Institute of Technology (NIT) Warangal along with an MBA from the prestigious Indian Institute of Management, Ahmedabad (IIMA). </p> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Megha Tata -->
<div class="modal fade" id="megha_tata" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Megha Tata   <br />
					<p> Managing Director – South Asia <br /> Discovery </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> A media & entertainment industry veteran, Megha has held leadership roles at organisations such as BTVI, HBO, Turner International and Star TV, in an illustrious career spanning more than 28 years. </p>
									 
				<p> Megha currently leads multiple national as well as international industry forums.  She was recently coopted as the Director on board of Indian Broadcasting Foundation. She is on board of the National Media & Entertainment Committee of FICCI and is the Vice President of the Indian chapter of the International Advertising Association (IAA).  </p>
				 
				<p> Megha has also been Jury member of leading industry awards like International Emmys, Promax, Children's Film Festival, Golden Mikes Awards, International Women in Sales Awards, Impact's 30 under 30 and many more. Besides being involved in industry initiatives, she feels energized by engaging with young minds through guest lectures at leading educational institutes including IIT, IIM, ISB, UBS and more. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Megha Tata -->
<div class="modal fade" id="pranjal_sharma" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					 Pranjal Sharma   <br />
					<p> Economic Analyst and Author, India Automated </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Pranjal Sharma is an economic analyst, advisor and author who focuses on technology, globalisation and media. He serves on boards of enterprises and non-profit entities which are leveraging emerging technologies for sustainable, equitable growth. He has written and edited several reports and papers on economic development. He is a columnist with Business Standard  newspaper. </p>
				<p> His new book is India <strong>Automated</strong>: How the Fourth Industrial Revolution is Transforming India. </p>
				<p> He served as Advisor Strategy to India's public service broadcaster Prasar Bharati, (Ministry of Information & Broadcasting) for two years where he helped bring in industry best practices and enabled creation of digital media teams. </p>
				<p> Previously, he spent more than two decades in print, internet and TV media, mostly in leadership roles with focus on India's economic engagement with the world. As Founding Executive Editor at <strong>Bloomberg UTV</strong>, he helped launch and run the channel. At TV Today Network, his team pioneered business news content for non-English audiences. Pranjal received the News Television Award for best business show in 2007. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Megha Tata -->
<div class="modal fade" id="ritu_kapur" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Ritu Kapur  <br />
					<p> CO-FOUNDER AND CEO, QUINTILLION MEDIA (TheQuint) </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>  RituKapur is the co-founder and CEO of The Quint, an independent news site in India. She has driven digital innovation - from The Quint innovation lab to launching a fact check initiative - WebQoof, that crowdsources fake news. </p>
				<p> Ritu has also strived to provide multiple platforms for free speech like The Quint's citizen journalism initiative "My Report", "Talking Stalking" – a campaign to change the laws to make stalking a non-bailable offence and "Me The Change" that focusses on the rights of young women in India. </p>
				<p> Ritu spent over two decades in broadcast as the founder of Network 18 where she won awards for a docudrama series "Bhanwar" and for The Citizen Journalist show among others. At Network 18 she led programming on History TV 18 as well as was the Features Editor at CNN IBN, before she exited to launch The Quint. </p>
				<p> She is on the advisory board of Oxford University's Reuters Institute of Journalism, the World Editor's Forum at WAN IFRA and Future News Worldwide. RituKapur has been recognized by Outlook Business as "Woman of Worth 2017 - The Newsmaker". </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Roshan Abbas -->
<div class="modal fade" id="ravi_velhal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Ravindra Velhal
					<p> Global Content Strategist. Intel Corporation </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Ravindra "Ravi" Velhal leads Digital Media Standards, Technologies and Immersive Cinema programs globally at Intel. As a Cinema Virtual Reality Experience (VRE) pioneer, he co-produced Le Musk multi-sensory VR by A.R. Rahman, Save Every Breathe: Dunkirk VRE with Warner Bros, FIRST MAN: VRE with Universal Pictures and RYOT, directed Rio Carnival 2018 VRE with GloboTV, collaborated with Sony Pictures on SpiderMan: Home Coming VRE which was nominated for prestigious Emmy, and he recently launched Innovation Studios with Sony Pictures. At Mobile World Congress in Feb 2019, he collaborated with Sony Pictures to launch SpiderMan: Far From Home, the world's first multiplayer VRE gaming experience over 5G. He executive produced remake of Grease in Volumetric Cinema with Paramount pictures. His focus is to drive future immersive cinema, including future media formats worldwide. Ravi is recipient of Hollywood's prestigious Lumiere award twice, Infinity Film Festival of Beverly Hills Audience Choice Award. Ravi holds several global patents, and he has served as a director on the AIS/VR Society board, and an advisor to film and trade organizations, globally. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Roshan Abbas -->
<div class="modal fade" id="roshan_abbas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Roshan Abbas
					<p> Managing Director <br /> Geometry Encompass </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> One of India's finest orators and storytellers, Roshan Abbas is an actor; TV & radio host; producer; event manager; creative director; film & theatre director; lyricist; CEO; angel investor; serial entrepreneur; author; and public speaking coach. </p>
				
				<p> Roshan founded and led Encompass (now Geometry Encompass) as Managing Director into becoming India's largest, most awarded experiential marketing agency. He was also an initial investor in and continues to be a mentor-partner at The Glitch. Both ventures have been acquired by WPP. </p>
				
				<p> Roshan has won seven national-level awards for TV and radio. In 2015, he co-founded Kommune - a performing arts collective for storytellers. He also helped conceptualise Spoken festival, bringing together performers from across the world for storytelling, poetry, spoken word, and song. </p>
				
				<p> Roshan continues to inspire by sharing his life lessons on communication and storytelling with entrepreneurs, corporates, artists, and anyone looking to discover a unique voice!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Sam Carlisle -->
<div class="modal fade" id="sam_carlisle" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Sam Carlisle<br />
					<p> Senior Director, External Partner Relations, Microsoft </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Sam came to the games industry with a background as a consultant specialising in training businesses and the public sector in negotiation and strategic partnering. </p>
				<p> In the mid 90's he co-founded an indie comic book studio. In 2006 he joined Sumo Digital as an artist; taking on the role of Outsource Manager in 2009. Here he helped guide the success of Sumo's new Indian art studio, while overseeing the developer's rapidly expanding global outsource program. </p>
				<p> In 2014 Sam joined Microsoft as Sr. Outsource PM for the EMEA regions before becoming Dir. of External Partners Worldwide. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Shoojit Sirca -->
<div class="modal fade" id="shoojit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Shoojit Sircar  <br />
					<p> Film Director and producer </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Shoojit Sircar is an Indian film director and producer. He directed Yahaan (2005), Vicky Donor (2012), Madras Cafe (2013), Piku (2015), October (2018), Gulabo Sitabo (2020). He also produced the 2012 film Aparajita Tumi. He won the National Film Award for Vicky Donor in Best Popular Film Providing Wholesome Entertainment category, shared with John Abraham. He also won the Indian Competition Special Jury Award at Osian's Cinefan Festival of Asian and Arab Cinema in 2005 for Yahaan. He is now making a documentary on Amitabh Bachchan. He is associated with Rising Sun Films and has made television commercials for Saffola, 2G, Apsara Pencils, Fair & Lovely, Maruti WagonR, Cadbury, Dove, Zomato, and many other brands. </p>
				
				<p> He is known for making independent films. His 2012 film Vicky Donor was grand box office success, earning ₹134 million (equivalent to ₹200 million or US$2.9 million in 2019) in the opening weekend. His film, Madras Cafe, was released in 2013, followed by Piku in 2015 and October in 2018. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Stephan Ottenbruch -->
<div class="modal fade" id="stephan_ottenbruch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Stephan Ottenbruch <br />
					<p> Festival Director, Indo German Film Festival </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Stephan Ottenbruch has worked as an executive producer, producer and commissioning editor on numerous  Feature Films and Series for TV before he started to create Media-Events like "The Dream of Zanzibar" and the Indian Filmfestival "IndoGerman Filmweek" for which he is currently serving as a festival director at Babylon in Berlin. For IndoGerman Films he is developing Content, consulting and bridge building for several media companies between Germany and India. Since 2014 Stephan Ottenbruch is a board member of the German Academy for Television and since 2018 he is heading the organization of Masterclasses and producing the annual award ceremony for the German TV Awards. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Sunil Lulla -->
<div class="modal fade" id="sunil_lulla" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Sunil Lulla <br />
					<p> Chief Executive Officer <br /> BARC India </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Sunil Lulla brings over 35 years of significant leadership and domain proven knowledge, with ground-up experience in growing brands and building businesses. Having worked across Media, Brands and Advertising he has served in leadership roles at MTV, SONY, Times Television Network, SaReGaMa, Diageo, Indya.com, GREY group, JWT and Balaji Telefilms. Sunil maintains active interests in serving industry interests to foster the spirit of self-regulation and collaboration. He is an active long-distance runner. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- taejin_lee -->
<div class="modal fade" id="taejin_lee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
				Taejin Lee <br />
					<p> Regional Director for the Philippines at Korea Copyright Protection Agency </p>				
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Taejin Lee currently serves as Regional Director for the Philippines at Korea Copyright Protection Agency.</p>
				<p>  He currently focuses on extending international partnerships of KCOPA by establishing a mutually cooperative eco-system against piracy with public and private sectors at a global level. </p>
				<p>  Before joining KCOPA his scope of work at Korea Music Copyright Association, Line corporation, an instant messaging service company and JTBC, a comprehensive broadcasting company ranges not only from handling domestic copyright license request with coordination to achieve full compliance with laws and regulations in Korea to taking actions against copyright violations as well as managing more than +60 reciprocal representation agreements with collective management organizations across nations. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Taran Adarsh -->
<div class="modal fade" id="taran_adarsh" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
				Taran Adarsh <br />
					<p> Active Film Critic and Business Analyst since four Decades </p>				
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
				<p>  Taran Adarsh, age 55, is an active film critic and business analyst since four decades. Starting his career at the age of 15 with 'Trade Guide', founded by his father Shri BK Adarsh, Taran diversified to television and web subsequently. Taran contributes reviews and Boxoffice analysis to 'Bollywood Hungama', the leading website, since two decades. Also, Taran is very active on social media and has the maximum following [film journalists] on various platforms: Twitter [3.7 million], Facebook [1.1 million] and Instagram [427k]. Taran is the most quoted film journalist today, with the most reputed websites and channels quoting his BO data and news developments on a daily basis. Taran has conducted masterclasses and moderated QnA sessions at various international film festivals in India and Overseas. </p>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Tarsame Mittal -->
<div class="modal fade" id="tarsame_mittal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Tarsame Mittal <br />
					<p> Founder - TM Talent Management Organisation </p>				
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>  TM Talent Managementis one of the most preferred Talent Management Organization in the Indian Music Industry. It represents 23 musicians from the Indian Music & Live Entertainment Industry including some of the most renowned names like Ajay-Atul, Amitabh Bhattacharya, Amit Trivedi, Arijit Singh, Hariharan, Rekha Bhardwaj, Sukhwinder Singh, Vishal Bhardwaj, Vishal & Shekhar, and Zubeen Garg. </p>
				<p> Truly Comical - A unit of TM Talent Management focused on representing comical talent exclusively from India. TC exclusively manages Kapil Sharma Live (the most popular comic act from India) and Comedy Nights Live (customized and specially curated comedy acts by some of the best comedy writers). </p>
				<p> Entertainment Consultant - A one-stop solution for allentertainment requirements for any event. </p>
				<p> TM Music -TM Music is an artiste first Music Label which believes in keeping artistes at the forefront. We aim to create & promote good non film music with passionate and talented artistes, who are either undiscovered or are emerging. We are making an honest attempt to create a platform for these talents and support the non-film music movement. Music Industry Professional Atul Churamani, Celebrated Music Producer & Composer Sunny M.R. and Entrepreneur Tarsame Mittal is leading the initiative.  </p> 									
				<p> Truly Musical LLP - A division of T M Talent Management that has been focusing on creating unique & meaningful concepts around music. The initiatives include: - </p>
				<p>  All About Music  is an all-inclusive B2B music conference"The conference is a global gateway into the Indian Music markets and has been connecting the dots of Indian Music Industry since 2017". All About Music has successfully completed 3 sold-out editions, hosted over 300 Indian and Global music industry influencers and over 4000 delegates. </p>
				<p>  Bollywood Music Project - A two-day multi-stage & genre music festival that features over 60 artists from the Music Industry and has successfully completed 5 editions in the past 36 months. </p>
				<p>  Music Plus -An interactive cross-divisional music communication portal that focuses on each segment of the Indian music industry by bridging the gap between the consumer and the creator along with the entertainment and business side.  Music Plus also intends to cover the legal, finance, entertainment & business aspects of the global music industry. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Tarsame Mittal -->
<div class="modal fade" id="tarun_katial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
				CEO <br />
					<p> ZEE5 India </p>				
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>  A media veteran of over two decades, Tarun Katial is the CEO of ZEE5 India. In his current role, Tarun is responsible for steering ZEE5, India’s largest and most comprehensive digital entertainment platform for language content, towards gaining industry leadership position.  </p>

				<p> Considered one of the most successful executives in the Indian media industry, Tarun has a robust body of experience across media platforms including television, radio and now the newest kid-on-the-block – OTT. Prior to joining ZEE5, Tarun’s last assignment was with Big FM, where he was the Founder COO-CEO, and during this duration he also set up Thwink BIG – the content incubator, and the BIG TV channels - Magic and Ganga.</p>

				<p> An MBA-graduate, Tarun began his career with advertising agencies like Saatchi & Saatchi, Enterprise Nexus Lowe and Ogilvy & Mather. Pursuant to this, he moved to Star Network and rose to become the Head of Content and Communications across the network in India before moving to Sony Entertainment Television as Business Head.  </p> 	

				<p> Tarun has an established record of accomplishment through successful shows - KBC - Who wants to be a Millionaire - Lets make a deal India, a slew of daily fiction content at STAR and Indian Idol | Fear Factor | Fame Academy | Ugly Betty at SET. These have been in perfect tune with what captivates the Indian consumer. </p>
				<p>  Tarun’s acumen has been recognized at multiple Indian and international platforms. He has been voted as NewsCorp Achiever for Asia and included among the best in the ‘India Today 30 on 30’ list. He was also part of the team that won the first Media Gold at Cannes. Instrumental in revolutionizing the media and entertainment landscape in India, Tarun is now a Tedx speaker, and part of industry forums including Jury chair for the Ad club Abby and DMAI, Chairman of IAMAI’s Digital Entertainment committee; member with FICCI and CII, Ex-Com at IAAA and Vice President at Association of Radio Operators for India (AROI).
				</p>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Ted Schilowitz -->
<div class="modal fade" id="ted_schilowitz" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Ted Schilowitz <br />
					<p> CTO and Futurist, Paramount Pictures </p>				
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Ted spends his time and pursues his passion in a unique role in the motion picture entertainment industry.   </p>
 
				<p> As the Futurist at Paramount pictures, Ted works with studio leadership and the technology teams at Paramount / Viacom/CBS, exploring forms of new and emerging technologies, with an emphasis on Virtual and Mixed Reality.    </p>
				 
				<p> Prior to joining Paramount, Ted was the Futurist at 20th Century Fox, where he worked on the evolving art, science and technology of advanced interactive visual storytelling.  He was part of the creation team for the Martian VR and Wild VR experiences, which premiered at CES and Sundance as groundbreaking projects that pushed the envelope of Virtual Reality storytelling,   </p>
				 
				<p> In addition, Ted was part of the creation teams for the Alien, Planet of the Apes and Predator VR experiences at Fox, and at his current home at Paramount Pictures, he's been instrumental in bringing the Grease Mixed Reality experience, the Light as a Feather mixed reality experience and the Quiet Place Virtual Reality experience to life. 
				 
				<p> Ted was an integral part of the product development team at Red Digital Cinema as the company's first employee.  The Red One and Epic cameras have made a significant impact on the Motion Picture Industry, winning a scientific and technical academy award. Many of the world's biggest movies are now being shot with these ultra high resolution digital movie cameras.   </p>
				 
				<p> Ted is one of the founders and creators of the G-Tech product line of advanced hard drive storage products.  As one of the most recognized brands in that industry, they are implemented worldwide at the highest levels on cinema, episodic television, sports and news production.   </p>
				 
				<p> In his role as Chief Creative Officer at Barco Escape,  he spearheaded an experimental immersive cinema project for movie theaters worldwide.  Titles include Paramount Pictures Star Trek Beyond and 20th Century Fox Maze Runner & Maze Runner 2.  </p>
				 
				<p> Before being part of the founding teams at Red Digital Cinema and G-Tech, Ted was on the team that developed and launched the Macintosh desktop video division of AJA Video Systems, creating professional video products in tandem with Apple.  These products are used on a massive scale worldwide for video production and post at the highest levels, on many of the world's biggest movies, TV series and sporting events.  </p>
				 
				<p> Ted has presented worldwide at numerous conferences on the advancements in next generation visual experiences for the movie, television and, interactive entertainment industries.  He's been featured in Wired, Variety, NY Times, LA Times, Wall Street Journal, Fast Company, The Hollywood Reporter, NBC, CNET, Studio Daily, Videography, Film and Video, DV Magazine, TV Technology, HD Video Pro, Engadget, Gizmodo, Millimeter, American Cinematographer, MacWorld, Post Magazine, Popular Science, and countless other publications discussing his areas of passion and exploration. In 2019, Ted was honored at the Variety Hall of Fame event with the Variety Innovation Award.  </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Trevor Fernandes -->
<div class="modal fade" id="trevor_fernandes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Trevor Fernandes <br />
					<p> Vice President, Government Affairs Asia Pacific Region, Motion Pictures Association</p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Trevor Fernandes is the Vice President, Government Affairs, of the Asia-Pacific Region for the Motion Picture Association – whose global mission is to advance and support the film, television and streaming content industry.  </p>
 
				<p> Working directly with local stakeholders, government bodies and regulatory agencies, Trevor is responsible for developing and advocating policy initiatives which advance the MPA studios' interests in market access and investment while promoting and protecting creators' rights. </p>
				 
				<p> An experienced international studio executive, Trevor has broad commercial, business affairs, and finance expertise including ten years in the Asia Pacific region focused on building partnerships with theatrical exhibitors, content distribution and technology providers, and content creators. He has worked previously with Twentieth Century Fox International, The Walt Disney Company and EY. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Vanita Kohli-Khandekar -->
<div class="modal fade" id="vanita_kohli" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">
					Vanita Kohli Khandekar<br />
					<p> Contributing Editor <br /> Business Standard </p>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Vanita Kohli-Khandekar has been tracking the Indian media and entertainment business for almost two decades. </p> 
				<p> Currently she is a columnist and writer for one of India's leading financial dailies, Business Standard. She also writes for Singapore-based Content Asia. Her earlier stints include one at Businessworld and EY. A Cambridge University press fellow (2000), Vanita has taught at some of the top media schools in India. Her first book, The Indian Media Business (Sage), is in its fourth edition. Her second book, The Making of Star India (Penguin-Random House), was released in 2019. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Vikash Jaiswal  -->
<div class="modal fade" id="vikash_jaiswal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
				Vikash Jaiswal 
					<p> CREATOR of All Time Hit mobile game Ludo King. <br /> Founder and CEO of Gametion Technologies. </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Vikash Jaiswal is the CREATOR of All Time Hit mobile game Ludo King. He is founder and CEO of Gametion Technologies, a Navi Mumbai based game development company. </p>

				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Vivek Singh  -->
<div class="modal fade" id="vivek_singh" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Vivek Singh 
					<p> Jt. MD <br /> Procam International Pvt Limited </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Vivek Singh is a pioneer in the field of sports management.  Along with brother Anil, they started Procam International in 1988. Procam is India's leading sports promoter and has carved out a niche for itself, in live events, sports consultancy and television production. </p>

				<p> The Tata Mumbai Marathon (in the top 10 Marathons of the world), the Airtel Delhi Half Marathon, the Tata Consultancy Services World 10K Bangalore and the Tata Steel Kolkata 25K Run are some of the largest running events in the world today.  </p>

				<p> In addition to motivating thousands in India, to take on a healthier and fitter lifestyle, the Marathons have raised millions of Dollars for different charitable causes. They have underscored the fact that private commercial initiative and social interests are complementary.  </p>

				<p> In March 2017, Procam broke new ground with an unparalleled initiative - The NEXA P1 Powerboat Grand Prix of the Seas. Conducted in Mumbai at the Marine Drive bay - the event had the world's best drivers and navigators racing P1 Panther boats inside a specially constructed racecourse on water. This heralded a new sporting paradigm in India and provided the world with a new racing format. </p>

				<p> A graduate in economics from Bombay University, his business sense, eye for detail and hands on approach make him a respected leader of the sports industry. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Vynsley Fernandes   -->
<div class="modal fade" id="vynsley_fernandes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"> 
					Vynsley Fernandes  
					<p> Chief Executive Officer <br /> IndusInd Media & Communications Ltd.. </p> 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p> Vynsley Fernandes is the Chief Executive Officer of IndusInd Media & Communications Ltd. (IMCL) - the flagship media business of the global Hinduja Group. He is a veteran media executive and one of India's foremost technocrats - with just under 30 years of experience in delivering and managing assignments globally - including DTH (Direct-to-Home) & HITS (Headend-In-The-Sky) platforms; digital cable networks; and news & entertainment channels. IMCL runs two state-of-art digital content distribution platforms - InDigital (a digital cable network) and NXT Digital - a satellite-based HITS platform. The two platforms together have a footprint in over 1,500 cities and towns in India. Along with its subsidiary broadband business, IMCL is the only integrated digital delivery platform in the country available through cable, satellite and broadband. Prior to joining IMCL, Vynsley was the Executive Director at CastleMedia; a leading technology, media & entertainment consulting-to-delivery firm which he co-founded in 2010. At CastleMedia, Vynsley led the consulting practice, focussed on pay television platforms across the SAARC, the Middle East and Africa. Prior to CastleMedia, Vynsley has managed high profile assignments for leading blue-chip media organisations including 21st Century Fox (then News Corporation), TATA SKY and ABP News. He also spent a decade at STAR India managing key technology and operations portfolios. With an academic background in Mass Communications and Media, Vynsley is considered a thought leader in the media & entertainment space and spearheads the Hinduja Group's media ventures of IMCL.  </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
