@include('frontend.layout.header')

	<br /><br />
	<section class="register_form">
		<div class="container">
            <div class="row justify-content-md-center"> 
				<div class="col-12">
					<h2 class="text-center"> Confirmation </h2>
					<div class="title-border"><span></span></div>
				</div> <?php // print_r($data);?>
                
				<div class="col-lg-10"> 
					<div class="card"> 
						<form name="form1" method="post" action="http://payment.ficci.com/frames/paymentprocess.asp"   class="contact100-form validate-form">
						@csrf                           
						<input type="hidden" name="RegId" value="{{$data['RegId']}}" >
						<div class="row">
							<div class="col-sm-6 form-group required">
								<label class="bold"> Nationality </label>
								<input name="nationality" class="form-control" maxlength="255" type="text" value="{{$data['nationality']}}"  placeholder="Organisation*" id="Organisation" required="required" />
							</div>
							<div class="col-sm-6 form-group required">
								<label class="bold"> Organisation </label>
								<input name="Organisation" class="form-control" maxlength="255" type="text" value="{{$data['Organisation']}}"  placeholder="Mailing Address*" id="Address"  required="required" />
							</div>
                          </div>
                          <div class="row">
							<div class="col-sm-6 form-group required">
								<label class="bold"> Designation </label>
								<input name="ContactName" class="form-control" maxlength="255" type="hidden" value="{{$data['Name'][0]}}"  placeholder="Organisation*" id="Organisation" required="required" />
								<input name="Designation" class="form-control" maxlength="255" type="text" value="{{$data['Designation'][0]}}"  placeholder="Organisation*" id="Organisation" required="required" />
							</div>

							<div class="col-sm-6 form-group required">
								<label class="bold"> Country </label>
								<input name="country" class="form-control" maxlength="255" type="text" value="{{$data['country']}}"   id="country"  required="required" />
							</div>
                          </div>
                          <div class="row">
						  @if(isset($data['State']))
							<div class="col-sm-6 form-group required">
								<label class="bold"> State </label>
							   <input name="State" class="form-control" maxlength="255" type="text" value="{{$data['State']}}"  placeholder="State*" id="State" required="required" />
							</div>
							@endif
							<div class="col-sm-6 form-group required">
								<label class="bold"> City </label>
								<input name="City" class="form-control" maxlength="255" type="text" value="{{$data['City']}}"   id="City"  required="required" />
							</div>
                          </div>

                          <div class="row">
							<div class="col-sm-6 form-group required">
								<label class="bold"> ZipCode </label>
							   <input name="ZipCode" class="form-control" maxlength="255" type="text" value="{{$data['ZipCode']}}"  placeholder="ZipCode*" id="ZipCode" required="required" />
							</div>

							<div class="col-sm-6 form-group required">
								<label class="bold"> OfficialEmail </label>
								<input name="OfficialEmail" class="form-control" maxlength="255" type="text" value="{{$data['OfficialEmail']}}"   id="OfficialEmail"  required="required" />
							</div>
                          </div>

						 <div class="row">
							<div class="col-sm-6 form-group required">
								<label class="bold"> Number Of delegate(s) </label>
							   <input name="quantity" class="form-control" maxlength="255" type="text" value="{{$b}}"  placeholder="quantity*" id="quantity" required="required"  readonly />
							</div>

							<div class="col-sm-6 form-group required">
							@if($data['nationality']=='Foreign National')
							<label class="bold"> Amount</label>
							<?php $tamount = $b*$data['Amount'];  ?>
								<input name="tamount" class="form-control" maxlength="255" type="text" value="{{$tamount}}"   id="Amount"  required="required" readonly />
								<input name="Amount" class="form-control" maxlength="255" type="hidden" value="{{$data['Amount']}}"     required="required"  />
							@else
								<label class="bold"> Amount with 18% GST</label>
								<?php $tamount = $b*$data['Amount']; $Amtgst =$tamount + ($tamount/100)*18; ?>
								<input name="tamount" class="form-control" maxlength="255" type="text" value="{{$Amtgst}}"   id="Amount"  required="required" readonly />
								<input name="Amount" class="form-control" maxlength="255" type="hidden" value="{{$data['Amount']}}"     required="required"  />
							@endif	
							</div>
                          </div>

							<div class="submit">								
								<input type="hidden" name="dynamicText" value="1" />
                                   
								<input class="form-control" type="hidden" name="pageurl" value="{{ url('/success') }}" />
                                <input type="submit" name="Submit" value="Check Out"  class="btn w-100 primary-btn"  onClick="return validate()"> 
							</div>
						</form>					
					</div> <!-- -->
                </div> <!-- -->
            </div> <!-- Row -->
        </div> <!-- Container -->
	</section>
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
			<div class="row no-gutters supporters-wrap clearfix">
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/star.jpg" class="img-fluid" alt=""> <p> Star - Convention partner </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/italy.jpg" class="img-fluid" alt=""> <p> Italy- Partner Country </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/intel.jpg" class="img-fluid" alt=""> <p> Intel- Diamond partner & BAF Awards  Co-Presenter</p>  </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/netflix.jpg" class="img-fluid" alt=""> <p> NETFLIX- Gold partner </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/mpa.jpg" class="img-fluid" alt=""> <p> Motion Picture Association Pvt ltd.(MPA) - Associate partner </p> </div>
				</div>
			</div>
		</div>
	</section>
	
@include('frontend.layout.footer')
