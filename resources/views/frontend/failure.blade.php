@include('frontend.layout.header')
	<br />
	<section id="success">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-lg-10">
					<div class="card shade d-block text-center">
						<img src="/img/payment_failed.jpg" class="img-fluid mx-auto d-block" width="300" />
						<h1 style="font-size:40px; margin:10px 0;"> Failure </h1>
						<p style="font-size:18px; margin-bottom:5px;"> Your transaction is failed please try again.</p>
						<br />
						<a href="/registration" class="primary-btn top-btn">TRY AGAIN</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@include('frontend.layout.footer')

