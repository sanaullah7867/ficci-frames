@extends('layouts_admin.app')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2> Change Password </h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="dashboard"> Home </a>  </li>
            <li class="breadcrumb-item"> Administration </li>
            <li class="breadcrumb-item"> Change Password </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="/admin/user"><button class='btn btn-info'> List Users</button></a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">			
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5 class="mb-0"> <a href="#" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> Change Password </h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row justify-content-md-center">			
							<div class="col-lg-7 col-md-8">
								<div class="card">
									<form>
										<div class="form-group required">
											<label for="UserPassword">Current Password</label>
											<input name="data[User][password]" class="form-control" type="password" id="UserPassword" required="required">
										</div>
										<div class="form-group required">
											<label for="UserPassword"> New Password</label>
											<input name="data[User][password]" class="form-control" type="password" id="UserPassword" required="required">
										</div>
										<div class="form-group required">
											<label for="UserPassword"> Confirm  Password</label>
											<input name="data[User][password]" class="form-control" type="password" id="UserPassword" required="required">
										</div>
										<div class="submit">
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										</div>
									</form>
								</div>
								<br />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts_admin.admin_footer')
</div> <!-- page-wrappe -->
@endsection
