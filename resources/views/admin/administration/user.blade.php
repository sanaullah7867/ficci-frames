@extends('layouts_admin.app')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2> Users List </h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="dashboard"> Home </a>  </li>
            <li class="breadcrumb-item"> Administration </li>
            <li class="breadcrumb-item"> Users </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="/admin/add_user"><button class='btn btn-info'> <i class="fa fa-plus" aria-hidden="true"></i> Add New Users</button></a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5 class="mb-0"> <a href="#" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> Users </h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<form action="" class="form-inline search-form">
									<div class="form-group">
										<label for="UserUsername">Username : </label>
										<input name="data[User][username]" class="form-control" placeholder="Username" maxlength="100" type="text" id="UserUsername">
									</div>
									<div class="form-group">
										<label for="UserGroupId">Group : </label>
										<select name="data[User][group_id]" class="form-control" placeholder="Username" id="UserGroupId">
											<option value="">--Select Group--</option>
											<option value="1">SuperAdmin</option>
											<option value="2">Managers</option>
										</select>
									</div>
									<div class="form-group">
										<label for="UserName">Name : </label>
										<input name="data[User][name]" class="form-control" placeholder="Name" type="text" id="UserName">
									</div>
									<div class="form-group">
										<label for="UserEmailAddress">Email Id : </label>
										<input name="data[User][email_address]" class="form-control" placeholder="Email Id" maxlength="100" type="text" id="UserEmailAddress">
									</div>
									<div class="form-group">
										<label for="UserIsActive">Is Active</label>
										<select name="data[User][is_active]" class="form-control" id="UserIsActive">
											<option value="">-- Select Status --</option>
											<option value="0">In Active</option>
											<option value="1">Active</option>
										</select>
									</div>
									<input class="btn btn-primary inline_submit" type="submit" value="Search">
								</form>
							</div>
						</div>
						<br />
						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover table-bordered table-striped">
										<tr>
											<th><a href="/admin/users/index/sort:id/direction:asc" class="desc">Id</a></th>
											<th><a href="/admin/users/index/sort:username/direction:asc">Username</a></th>
											<th><a href="/admin/users/index/sort:group_id/direction:asc">Group</a></th>
											<th><a href="/admin/users/index/sort:name/direction:asc">Name</a></th>
											<th><a href="/admin/users/index/sort:email_address/direction:asc">Email Address</a></th>
											<th><a href="/admin/users/index/sort:is_active/direction:asc">Status</a></th>
											<!--<th></th>-->
											<th><a href="/admin/users/index/sort:created/direction:asc">Created</a></th>
											<th class="actions">Actions</th>
										</tr>
										<tr>
											<td onclick="document.location='users/edit/1'">1&nbsp;</td>
											<td onclick="document.location='users/edit/1'">admin&nbsp;</td>
											<td onclick="document.location='users/edit/1'"> SuperAdmin </td>
											<td onclick="document.location='users/edit/1'">VZ Competitions  Volzero&nbsp;</td>
											<td onclick="document.location='users/edit/1'">info@volzero.com&nbsp;</td>
											<td onclick="document.location='users/edit/1'">
											<a href="/admin/users/status/1" class="status_active"><i class="fa fa-check"></i></a>&nbsp; </td>
											<!--<td onclick="document.location='users/edit/'">&nbsp;</td>-->
											<td onclick="document.location='users/edit/1'">12-06-2018&nbsp;</td>
											<td class="actions">
												<a href="/admin/users/view/1" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</a>
												<a href="/admin/users/edit/1" class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i> Edit</a>
											</td>
										</tr>
									</table><!-- /.table -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts_admin.admin_footer')
</div> <!-- page-wrappe -->
@endsection


