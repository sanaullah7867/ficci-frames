@extends('layouts_admin.app')
@section('content')
<?php
use App\Http\Controllers\StaticfunctionController;

  ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2> Promo COdes</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="/admin"> Home </a>  </li>
            <li class="breadcrumb-item"> promo Codes </li>

        </ol>
    </div>
    <!--<div class="col-sm-4">
       <div class="title-action">
            <a href="#"><button class='btn btn-info' data-toggle="modal" data-target="#exampleModalCenter"> <i class="fa fa-plus" aria-hidden="true"></i> Add  </button></a>
        </div>
     </div> -->
</div>
<style> .bkred{background:#ef9090; float:left;} .bkgreen{background:#88f388; float:left;  box-shadow: 1px 1px 1px 0px grey;}
.bkred,.bkgreen {padding: 9px;
    float: left;
    margin: 5px;
    color: black;
    font-size: 15px;
    text-align: center;
    font-weight: bold;
   }
</style>
<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
                        <h5 class="mb-0"> <a href="{{ URL::previous() }}" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a> Promo Codes List </h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">
					
						<br />
						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<div class="table-responsive">
									
                                            @foreach($promo as $value)
                                              @if($value->status ==1)  <div class="col-sm-1 bkred"> {{ $value->promo_codes}} </div> @endif 

                                              @if($value->status ==0)  <div class="col-sm-1 bkgreen"> {{ $value->promo_codes}} </div> @endif
                                                
                                          @endforeach
                                  
                                  
								</div>
                <br> 
                <div class="pull-right"><?php echo $promo->render(); ?> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

                                <!-- Modal -->
                                <div class="modal fade bd-guidline-modal-lsm" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
									<div class="modal-content">
									  <div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Add FAQ</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										  <span aria-hidden="true">&times;</span>
										</button>
									  </div>
									  <div class="modal-body">
                                      <form name="newfaq" method="post" action="{{url('/admin/faq/save')}}" >
									@csrf
                                    <div class="row" id="faq">
											<div class="form-group col-lg-12 required">
												<label for="question">Question</label>
												<input name="question" class="form-control" maxlength="150" type="text" id="question" required="required">
											</div>
											<div class="form-group col-lg-12 required">
												<label for="answer">Answer</label>
												<input name="answer" class="form-control" maxlength="300" type="text" id="answer"  required="required">
											</div>
										</div>

										<div class="submit">
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										</div>
									</form>
									  </div>
									  <div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

									  </div>
									</div>
								  </div>
								</div>

    @include('layouts_admin.admin_footer')
    <script>
        function edit_faq(id){
                       // alert(id);
                        $.ajax({
                        type: "get",
                        url: "/admin/faq/edit",
                        data: "id=" +id,
                        success: function(data){
                            $("#faq").html(data)
                           // console.log(data);
                            },
                        });
                    }
        </script>
</div> <!-- page-wrappe -->
@endsection


