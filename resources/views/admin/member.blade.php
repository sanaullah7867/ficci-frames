@extends('layouts_admin.app')
@section('content')
<?php
use App\Http\Controllers\StaticfunctionController;
use App\Http\Controllers\HomeController;
$city_list = HomeController::city();
$state_list = HomeController::state();
use App\Http\Controllers\AdminController;
  ?>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2> Member</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="/admin"> Home </a>  </li>
            <li class="breadcrumb-item"> Paid Registeration List</li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action pull-right" style="padding-top:0px;">
           <!-- <a href="{{url('/admin/total/registration/export/excel')}}"><button class='btn btn-info'> <i class="fa fa-plus" aria-hidden="true"></i> Download Excel </button></a>  -->
            <form action="{{url('/admin/paid/registration/export/excel')}}" method="get"  autocomplete="off" class="form-inline search-form">
				<div class="form-group" id="sandbox-container">
					<label for="from_date">From Date: </label>
					<input name="from_date" class="form-control" placeholder="from_date" @if(isset($_GET['from_date'])) value="{{$_GET['from_date']}}" @endif  type="text" id="from_date">
				</div>
				<div class="form-group" id="sandbox-container">
					<label for="to_date">To Date: </label>
					<input name="to_date" class="form-control" placeholder="to_date" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif  type="text" id="to_date">
				</div>
				<input class="btn btn-primary inline_submit" type="submit" value="Export Data">
            </form>
        </div>
        <div class="clear"></div>
     </div>
</div>

<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
                        <h5 class="mb-0"> <a href="{{ URL::previous() }}" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a>Paid Registeration List </h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">

								<div class="row justify-content-md-center">
                                    <div class="col-lg-12">
                                   <!--  <form action="" method="get" class="form-inline search-form">

                                         <div class="form-group">
                                                <label for="name">Name: </label>
                                                <input name="name" class="form-control" placeholder="Name" @if(isset($_GET['name'])) value="{{$_GET['name']}}" @endif maxlength="100" type="text" id="name">
                                            </div>

                                            <div class="form-group" >
                                            <label for="country">Select State </label>
                                            <select name="state" class="form-control"  onchange="get_city(this.value)" >
                                                <option value="">Select State</option>
                                                @foreach($state_list as $slist)
                                                <option value="{{$slist->state_id}}" @if(isset($_GET['state']) && $_GET['state']==$slist->state_id) selected @endif> {{$slist->name}} </option>
                                                @endforeach
                                        </select>
                                        </div>

                                        <div class="form-group" >
                                            <label for="country">Select City </label>
                                            <select name="city" class="form-control" id="city_id" >
                                                @if(isset($_GET['state']) && $_GET['state']!='')
                                                    <?php //  $city_list = HomeController::citystate($_GET['state']); ?>
                                                    <option value="">Select city</option>
                                                        @foreach($city_list as $clist)
                                                        <option value="{{$clist->city_id}}" @if(isset($_GET['city']) && $_GET['city']==$clist->city_id) selected @endif> {{$clist->name}} </option>
                                                        @endforeach
                                                @else
                                                    <option value="">Select City </option>
                                                    <option value="">Please select state first </option>
                                                @endif
                                            </select>
                                        </select>
                                        </div>
                                             <input class="btn btn-primary inline_submit" type="submit" value="Search">
                                        </form>  -->
                                    </div>
                                </div>
                                <br />

						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover table-bordered table-striped">
                                    <tr>
											<th><a href="#" class="desc"> # </a></th>
                                            <th><a href="#">Date</a></th>
											<th><a href="#">RegId</a></th>
                                            <th><a href="#">Email</a></th>
                                            <th><a href="#">Contact</a></th>
											<th><a href="#">Nationality</a></th>
											<th><a href="#">Type Industry</a></th>
                                            <th><a href="#">Delegates</a></th>
											<th><a href="#">Organisation</a></th>
											<th><a href="#">No of Delegates</a></th>
											<th><a href="#">Amount</a></th>
											<th><a href="#">Trans. ID</a></th>
											<th><a href="#">Invoice No</a></th>
											<th><a href="#">Country</a></th>
											<th><a href="#">City</a></th>
                                          <!--  <th><a href="#">Action</a></th>  -->
									    </tr>
                                        <?php // print_r($user); exit;?>
                                        @foreach($user as $value)
                                      <tr>
                                      <td>{{ $i++ }}</td>
                                              <td> {{ date('d-m-Y | H:i:A',strtotime($value->created_at))}}  </td>
                                              <td> {{$value->RegId}} </td>
                                              <td> {{ $value->email }}  </td>
                                              <td> {{ $value->Mobile }}  </td>
                                              <td> {{ $value->nationality }}  </td>
											  <td> {{ $value->typeIndustry }}  </td>
											  <td> <?php $delegate = AdminController::getdelegates($value->id); $d=1; ?>
											  			@foreach($delegate as $del)
														  <p><b>{{$d}}- {{$del->name}} <b> </p>
														  <?php $d++;?>
														  @endforeach </td>
											  <td> {{ $value->Organisation }}  </td>
											  <td> {{ $value->quantity }}  </td>
											  <td> {{ $value->Amount }} + GST </td>
											  <td> {{ $value->txnId }}  </td>
											  <td> {{ $value->invoice_no }}  </td>
											  <td> {{ $value->country }}  </td>
											  <td> {{ $value->city }}  
                                             <!-- <a onclick="return confirm('Are you sure?')" href='{{url("/admin/member/delete/$value->id")}}' class="btn btn-danger btn-sm"><i class="fa fa fa-trash-o"></i></a>  -->
                                              </td>
                                           <!-- <td class="actions">

                                        <a onclick="return confirm('Are you sure?')" href='{{url("/admin/value/delete/$value->id")}}' class="btn btn-danger btn-sm"><i class="fa fa fa-trash-o"></i></a>
                                            </td> 
                                            <td class="actions">
                                            <a  href='{{url("/admin/activity/view/$value->id")}}' class="btn btn-primary btn-sm">View<i class="fa fa fa-view-o"></i></a>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </table> <!-- /.table -->
                                    <div class="pull-right"><?php echo $user->render(); ?> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

                                <!-- Modal -->
                                <div class="modal fade bd-guidline-modal-lsm" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
									<div class="modal-content">
									  <div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Add FAQ</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										  <span aria-hidden="true">&times;</span>
										</button>
									  </div>
									  <div class="modal-body">
                                      <form name="newfaq" method="post" action="{{url('/admin/faq/save')}}" >
									@csrf
                                    <div class="row" id="faq">
											<div class="form-group col-lg-12 required">
												<label for="question">Question</label>
												<input name="question" class="form-control" maxlength="150" type="text" id="question" required="required">
											</div>
											<div class="form-group col-lg-12 required">
												<label for="answer">Answer</label>
												<input name="answer" class="form-control" maxlength="300" type="text" id="answer"  required="required">
											</div>
										</div>

										<div class="submit">
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										</div>
									</form>
									  </div>
									  <div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

									  </div>
									</div>
								  </div>
								</div>

    @include('layouts_admin.admin_footer')
    <script>
      function get_city(id)
 {
     //alert(id);

    $.ajax({
          beforeSend: function() {
            $("#city_id").append("<img id='theImg' src='https://asn2020.eventregistration.in//img/loading.gif'/>");
        },
       type: "get",
       url: "/get_city",
       data: "city_id=" + id,
       success: function(data){
           $("#city_id").html(data);
	   },
         complete: function() {
            $("#wait").css("visibility", "hidden")
        }
    });
 }

        </script>
</div> <!-- page-wrappe -->
@endsection


