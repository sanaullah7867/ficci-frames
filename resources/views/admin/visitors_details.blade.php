@extends('layouts_admin.app')
@section('content')
<?php
use App\Http\Controllers\StaticfunctionController;

  ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2> Activity Details</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="/admin"> Home </a>  </li>
            <li class="breadcrumb-item"> Activity Details </li>

        </ol>
    </div>
    <!--<div class="col-sm-4">
       <div class="title-action">
            <a href="#"><button class='btn btn-info' data-toggle="modal" data-target="#exampleModalCenter"> <i class="fa fa-plus" aria-hidden="true"></i> Add  </button></a>
        </div>
     </div> -->
</div>

<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
                        <h5 class="mb-0"> <a href="{{ URL::previous() }}" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a> Activity Details List </h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<!-- <form action="{{url('/admin/total/activity/export/excel')}}" method="get"  class="form-inline search-form">

                                    <div class="form-group" id="sandbox-container">
                                        <label for="from_date">From Date: </label>
                                        <input name="from_date" class="form-control" placeholder="from_date" @if(isset($_GET['from_date'])) value="{{$_GET['from_date']}}" @endif  type="text" id="from_date">
                                    </div>
                                    <div class="form-group" id="sandbox-container">
                                        <label for="to_date">To Date: </label>
                                        <input name="to_date" class="form-control" placeholder="to_date" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif  type="text" id="to_date">
                                    </div>
                                    <input class="btn btn-primary inline_submit" type="submit" value="Export Data">
                                    </form>
							</div> -->
						</div>
						<br />
						<div class="row justify-content-md-center">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover table-bordered table-striped">
										<tr>
                                            <th><a href="#" class="desc"> # </a></th>
                                            <th><a href="#"> Name</a></th>
                                            <th><a href="#">Register At</a></th>
                                            <th><a href="#">Login At</a></th>
                                            <th><a href="#">In Lobby</a></th>
                                            <th><a href="#">In Auditorium</a></th>
                                             <th><a href="#">Logout At</a></th>
                                        </tr>
                                        @foreach($data as $value)
                                        <?php
                                        //$parti->participants_id
                                       // $users = StaticfunctionController::user_name($parti->user_id);

                                        ?>
										<tr>
                                            <td>{{ $i++ }}</td>
                                            <td> {{ $users = StaticfunctionController::user_name($value->user_id)}} </td>
                                            <td> {{ date('d-m-Y | H:i:A',strtotime($value->created_at))}} </td>
                                            <td> {{ date('d-m-Y | H:i:A',strtotime($value->login_at))}}  </td>
                                            <td> {{ date('d-m-Y | H:i:A',strtotime($value->in_loby))}} </td>
                                            <td> {{ date('d-m-Y | H:i:A',strtotime($value->in_auditorium))}}  </td>
                                            <td> {{ date('d-m-Y | H:i:A',strtotime($value->logout_at))}} </td>

                                         </tr>
                                        @endforeach
                                    </table> <!-- /.table -->
                                    <div class="pull-right"><?php echo $data->render(); ?> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

                                <!-- Modal -->
                                <div class="modal fade bd-guidline-modal-lsm" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
									<div class="modal-content">
									  <div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Add FAQ</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										  <span aria-hidden="true">&times;</span>
										</button>
									  </div>
									  <div class="modal-body">
                                      <form name="newfaq" method="post" action="{{url('/admin/faq/save')}}" >
									@csrf
                                    <div class="row" id="faq">
											<div class="form-group col-lg-12 required">
												<label for="question">Question</label>
												<input name="question" class="form-control" maxlength="150" type="text" id="question" required="required">
											</div>
											<div class="form-group col-lg-12 required">
												<label for="answer">Answer</label>
												<input name="answer" class="form-control" maxlength="300" type="text" id="answer"  required="required">
											</div>
										</div>

										<div class="submit">
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										</div>
									</form>
									  </div>
									  <div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

									  </div>
									</div>
								  </div>
								</div>

    @include('layouts_admin.admin_footer')
    <script>
        function edit_faq(id){
                       // alert(id);
                        $.ajax({
                        type: "get",
                        url: "/admin/faq/edit",
                        data: "id=" +id,
                        success: function(data){
                            $("#faq").html(data)
                           // console.log(data);
                            },
                        });
                    }
        </script>
</div> <!-- page-wrappe -->
@endsection



