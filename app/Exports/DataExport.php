<?php

namespace App\Exports;
use App\User;
use App\Address_others;
use App\Apply_promos;
use App\Competitions;
use App\Countries;
use App\email_templates;
use App\Enquiries;
use App\Groups;
use App\Juries;
use App\Occupations;
use App\Participant_as;
use App\Participants;
use App\Payment_gatways;
use App\payment_statuses;
use App\Promo_codes;
use App\Results;
use App\States;
use App\subscriptions;
use App\Total_scores;
use App\Uploads;
use Maatwebsite\Excel\Concerns\FromCollection;

class DataExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all();
    }
}
