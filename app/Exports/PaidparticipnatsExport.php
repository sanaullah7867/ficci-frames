<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;
use DB;
use App\Businessctg;
use App\Fdays;
use App\Delegates;
use App\Promocodes;
class PaidparticipnatsExport implements FromCollection,WithHeadings
{


    public function __construct(string $fromdate, string $todate)
    {
        $this->fromdate = $fromdate;
        $this->todate = $todate;
    }
    
    public function collection()
    {//print_r($this->year); print_r($this->date); exit;
        $from =$this->fromdate;
        $to = $this->todate;
// whereBetween('reservation_from', [$from, $to])
        if($this->fromdate !=''){
           // $from= '2020-05-29 19:00:00';
           // $to= '2020-05-29 23:59:59';
            $users =User::select('created_at','RegId','email','Mobile','nationality','typeIndustry',
            'MembershipNo','Organisation','quantity','Amount','txnId','invoice_no','country','city')
        ->whereBetween('users.created_at', [$from, $to])
        ->where('invoice_no','!=','')
       ->orderby('users.created_at', 'asc')
        ->get();

        }else{
       $users =User::select('created_at','RegId','email','Mobile','nationality','typeIndustry',
       'MembershipNo','Organisation','quantity','Amount','txnId','invoice_no','country','city')
       ->where('invoice_no','!=','')
       ->orderby('users.created_at', 'asc')
        ->get();
        }


        return $users;
    }

    public function headings(): array
    {
        return [
            'Registration Date',
            'RegId',
            'Email',
            'Contact',
            'Nationality',
            'Type Industry',
            'Membership No',
            'Organisation',
            'No of Delegates',
            'Amount',
            'Trans. ID',
            'Invoice No',
            'Country',
            'City',
			
		];
    }
}
