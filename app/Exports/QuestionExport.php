<?php

namespace App\Exports;

use App\Questions;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class QuestionExport implements FromCollection,WithHeadings
{
    public function collection()
    {
        $users =Questions::leftjoin('users', 'users.id', '=', 'questions.user_id')
	    ->select('users.name','question','questions.status','questions.created_at')
       ->orderby('questions.created_at', 'asc')
        ->get();

        return $users;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Questions',
            'Status',
            'Date',

		];
    }

}
