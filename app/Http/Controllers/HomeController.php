<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Cities;
use App\States;
use App\User;
use App\Businessctg;
use App\Fdays;
use App\Delegates;
use App\Promocodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
class HomeController extends Controller
{
    
    public function index()
    {   if(isset(Auth::user()->id)){
        Auth::logout();
        Session::flush(); }
        return view('frontend.index');
    }


    public function save_register(Request $request)
    {
        //B2bmeetings::create($request->all());
		//print_r($_POST); exit;
		 //$data = $request->all();
           // print_r(Input::all()); exit;

           $pswfrrd= rand(10000,99999);
            $quantity =count($request->Name);
           if(isset($request->Promocode)) {
            $cpromo =Promocodes::where('promo_codes',$request->Promocode)
            ->where('status',0)->get();
            
                if(count($cpromo) > 0){
                    //print_r(count($cpromo)); exit;

                        $user = new User();
                        $user->RegId = $request->RegId;
                        $user->nationality = $request->nationality;
                        $user->email = $request->OfficialEmail;
                        $user->typeIndustry = $request->typeIndustry;
                        $user->MembershipNo = $request->MembershipNo;
                        $user->Organisation = $request->Organisation;
                        $user->Address = $request->Address;
                        $user->country = $request->country;
                        $user->state = $request->State;
                        $user->city = $request->City;
                        $user->password = Hash::make($pswfrrd);
                        $user->password_view = $pswfrrd;
                        $user->zipcode = $request->ZipCode;
                        $user->gstny = $request->gstny;
                        $user->gstnumber = $request->GSTNumber;
                        $user->Telephone = $request->Telephone;
                        $user->Mobile = $request->Mobile;
                        $user->Fax = $request->Fax;
                        $user->Website = $request->Website;
                        $user->personal_profile = $request->personal_profile;
                        $user->current_business_interest = $request->current_business_interest;
                        $user->dynamicText = $request->dynamicText;
                        $user->Amount = $request->Amount;
                        $user->quantity = $quantity;
                        $user->invoice_no = 'PROMO_'.$request->Promocode;
                        $user->save();

            Promocodes::where('promo_codes',$request->Promocode)
                ->update(array('status' =>1));

                } else {
                    echo "<script>alert('Wrong Promocode'); </script>";
                    return redirect()->back();
                }


           } else {
               
            $user = new User();
            $user->RegId = $request->RegId;
            $user->nationality = $request->nationality;
            $user->email = $request->OfficialEmail;
            $user->typeIndustry = $request->typeIndustry;
            $user->MembershipNo = $request->MembershipNo;
            $user->Organisation = $request->Organisation;
            $user->Address = $request->Address;
            $user->country = $request->country;
            $user->state = $request->State;
            $user->city = $request->City;
            $user->password = Hash::make($pswfrrd);
            $user->password_view = $pswfrrd;
            $user->zipcode = $request->ZipCode;
            $user->gstny = $request->gstny;
            $user->gstnumber = $request->GSTNumber;
            $user->Telephone = $request->Telephone;
            $user->Mobile = $request->Mobile;
            $user->Fax = $request->Fax;
            $user->Website = $request->Website;
            $user->personal_profile = $request->personal_profile;
            $user->current_business_interest = $request->current_business_interest;
            $user->dynamicText = $request->dynamicText;
            $user->Amount = $request->Amount;
            $user->quantity = $quantity;
            $user->save();

        }    
        $uid = User::select('id')->where('Regid',$request->RegId)->first();
        $user_id = $uid->id;

                $fdays =$request->category;
            for($k=0; $k<count($fdays); $k++){
                 $userctg = new Fdays();
                 $userctg->user_id = $user_id;
                 $userctg->fdays = $fdays[$k];
                $userctg->save();
            }
            $dname= $request->Name;
            $ddesign = $request->Designation;
            for($b=0; $b<count($ddesign); $b++){

                session()->put('dname',$dname[0]);

                $userd = new Delegates();
                $userd->user_id = $user_id;
                $userd->name = ucfirst($dname[$b]);
                $userd->designation = ucfirst($ddesign[$b]);
              $userd->save(); 
            }

                $bzctg = $request->b_int;
            for($k=0; $k<count($bzctg ); $k++){
                if($bzctg[$k] !=''){
                        $userb = new Businessctg();
                        $userb->user_id = $user_id;
                        $userb->bctg = $bzctg[$k]; ;
                        $userb->save();
                }
            }



      /*     $udata = User::select('email','Regid','password_view')->where('RegId',$request->RegId)->first();
            $dname = ucfirst(session()->get('dname'));

                        $data1 = [
                            'name' 	=> $dname ,
                            'email'=>$udata->email,
                            'Regid'=>$udata->Regid,
                            'digits'=>$udata->password_view,
                            'subject' => 'FICCI Registration',
                        ];

                    Mail::send('mails.otp', ['data1'=>$data1], function($message) use($data1){
                        $message->from('zakir@abcdesigns.in','FICCI');
                            $message->subject($data1['subject']);
                            $message->to($data1['email']);
                            $message->bcc('zakir@abcdesigns.in');
                            });  */
        
          $data = array();
            $data = $request->all();

            if(isset($request->Promocode)){
               
                    return redirect('/success');
               
                 } 
            else { 

                    if($request->nationality=='Foreign National'){
                        //Session::flash('success','Thank You for contacting us!!!!');
                        return redirect('/registration-success');

                    } else {
                        return view('frontend.confirmation',compact('data','b'));
                    }
            }
         // Session::flash('success','Thank You for contacting us!!!!');
        //return redirect()->back();
    
   
}


public function thankpost(Request $request)
{

    //print_r($_POST);
    //exit;
    
    $data = array(); $data = $_POST;
     if($data['Resp_Code']=='100'){ 
        $data = array(); $data = $_POST;
         User::where('RegId',$data['Regid'])
        ->update(array('totalpaid' =>$data['Amount'],'txnId' =>$data['TXnID'],'invoice_no' =>$data['Invoice_no']));

          /*  $udata = User::select('email','Regid','password_view')->where('RegId',$data['Regid'])->first();
            $dname = ucfirst(session()->get('dname'));

                        $data1 = [
                            'name' 	=> $dname ,
                            'email'=>$udata->email,
                            'Regid'=>$udata->Regid,
                            'digits'=>$udata->password_view,
                            'subject' => 'FICCI Registration',
                        ];

                    Mail::send('mails.otp', ['data1'=>$data1], function($message) use($data1){
                        $message->from('zakir@abcdesigns.in','FICCI');
                            $message->subject($data1['subject']);
                            $message->to($data1['email']);
                            $message->bcc('zakir@abcdesigns.in');
                            });  */

            return view('frontend.success');
    } else {
        return redirect('/failure');
    }
}


public function generatepromo()
{
    for($i=1; $i<=1000; $i++) {
    $codes = str_random(8);
     $pro = new Promocodes();
     $pro->promo_codes = $codes;
     $pro->save();
    }
}

public function chekpromo()
{
    $pid= $_GET['promo'];
    $data = Promocodes::where('promo_codes',$pid)->where('status',0)->get();
    if(count($data) <= 0){
        return response()->json(['status' => "success", 'message' =>  "Invalid Promocode." ]);
    } 
}


    public function get_city()
    {   $id= $_GET['city_id'];
        $city_list = Cities::where('state_id',$id)->get();
        return  view('ajax_pages.address',compact('city_list'));
    }

    public static function state()
    {
        $state = States::where('country_id','101')->get();
        return $state;

    }

    public static function city()
    {
        $city = Cities::where('state_id','22')->get();
        return $city;
    }

    public static function citystate($id)
    {
        $city = Cities::where('state_id',$id)->get();
        return $city;
    }






}
