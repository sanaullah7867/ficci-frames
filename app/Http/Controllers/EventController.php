<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Businessctg;
use App\Fdays;
use App\Delegates;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
class EventController extends Controller
{




    public function apiregdata()
    {
       $param = User::where('invoice_no','!=','')->with('fdays')->with('delegates')->with('businessctg')->get();
       if(count($param)>0) {
         return response()->json($param, 200);
       } else 
       {return response()->json(['status' => "success", 'message' =>  "No Content" ],200);}
    }

    public function foreigndata()
    {
        $param = User::where('invoice_no',null)->where('nationality','Foreign National')->with('fdays')->with('delegates')->with('businessctg')->get();
       if(count($param)>0) {
         return response()->json($param, 200);
       } else 
       {return response()->json(['status' => "success", 'message' =>  "No Content" ],200);}

    }





    // Auth::logout();
       // Session::flush();

     public function login(Request $request) 
     {  
         
        $data = User::select('id')->where('email',$request->email)->first();
        //$dataw = '2020-07-08';
       // $tdate = date('dS F Y',strtotime($dataw));
       // print_r($tdate); exit;
       $tdate = date('jS F Y');
      
       if($data === null) {
        Session::flash('error','Email id does not exist !!!');
        return redirect()->back();   
       } else {
                $days =  Fdays::select('user_id','fdays')->where('user_id',$data->id)->where('fdays',$tdate)->first();
                if ($days === null) {
                         Session::flash('error','You are not register for todays event !!!');
                    return redirect()->back();
                } else {
                            if(Auth::attempt(['email' => $request->email, 'password' =>$request->password])){
                                //User::where('id',Auth::user()->id)->update(array('login_at' => Carbon::now()));
                                Session::flash('success','Success fully login !!!');
                            return redirect()->back();
                            
                            }
                            else {
                                Session::flash('error','Incorect Password');
                                 return redirect()->back();
                                //return response()->json(['status' => "fail"]);
                            }
                        }
        }
    }



    public function accept($id)
    {
        Questions::where('id',$id)
        ->update(array('status' =>1,'updated_at' => Carbon::now()));
        Session::flash('success','Queastion Accepted');
        return redirect()->back();
    }




    public function inout(){
        $users =User::where('id',Auth::user()->id)->first();

        if($_GET['ttype']==2 && $users->login_at ==''){
            User::where('id',Auth::user()->id)->update(array('login_at' => Carbon::now())); }

        if($_GET['ttype']==0 && $users->in_loby ==''){
            User::where('id',Auth::user()->id)->update(array('in_loby' => Carbon::now())); }

            if($_GET['ttype']==1 && $users->in_auditorium ==''){
                User::where('id',Auth::user()->id)->update(array('in_auditorium' => Carbon::now())); }

                if($_GET['ttype']==3 && $users->logout_at ==''){
                    User::where('id',Auth::user()->id)->update(array('logout_at' => Carbon::now())); }


   // $obj = new Event_vistis();
   // $obj->user_id = Auth::user()->id;
   // $obj->room_type = $_GET['ttype'];
   // $obj->save();

    }

    public function logout()
    {
        User::where('id',Auth::user()->id)->update(array('logout_at' => Carbon::now()));
       // $obj = new Event_vistis();
   // $obj->user_id = Auth::user()->id;
   // $obj->room_type =3;
   // $obj->save();

        Auth::logout();
        Session::flush();
        return redirect('/');

    }
}
