<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Businessctg;
use App\Fdays;
use App\Delegates;

use Illuminate\Support\Carbon;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $total_paid = User::where('invoice_no','!=','')->count();
        $unpaid =User::where('invoice_no','=',Null)->count();
        //$todays = User::where('created_at', '>=', Carbon::today())->orderBy('id', 'desc')->where('invoice_no','!=','')->paginate(20); $i=1;
        $todays = User::where('invoice_no','!=','')->orderBy('id', 'desc')->paginate(20); $i=1;
        return view('admin.dashboard',compact('total_paid','unpaid','todays','i'));
    }


}
