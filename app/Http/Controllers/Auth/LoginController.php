<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
//use Socialite;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\User;
use App\Admin;
use App\Juries;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    protected $providers = [
        'github',
        'facebook',
        'google',
        'twitter'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        
    }

    public function showAdminLoginForm()
    {
        return view('auth.login', ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
            //print_r($_POST);
           // exit;
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
             return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }




    public function socialLogin($driver)
    {
        return Socialite::driver($driver)->redirect();
	}

	public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->stateless()->user();
		 } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }
         // check for email in returned user
        return empty( $user->email )
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
	}

	protected function sendSuccessResponse()
    {
        return redirect()->intended('/participants');
    }

	protected function sendFailedResponse($msg = null)
    {
        return redirect()->route('social.login')
            ->withErrors(['msg' => $msg ?: 'Unable to login, try with another provider to login.']);
    }


	protected function loginOrCreateAccount($providerUser, $driver)
    {

        // check for already has account
        $user = User::where('email', $providerUser->getEmail())->first();

        // if user already found
        if( $user ) {
            // update the avatar and provider that might have changed
            // update the avatar and provider that might have changed
            // $user->update([
                // 'name' => $providerUser->getName(),
                // 'image' => $providerUser->getAvatar(),
                // 'provider' => $driver,
                // 'provider_id' => $providerUser->getId(),
                // 'access_token' => $providerUser->token
            // ]);

		DB::table('users')
        ->where('email', $providerUser->getEmail())
		->update(array('name' =>$providerUser->getName(),'image' => $providerUser->getAvatar(),'provider'=>$driver, 'provider_id'=>$providerUser->getId(), 'remember_token'=>$providerUser->token));

        } else {
            // create a new user
            if($driver!='facebook'){
                $user = User::create([
               'name' => $providerUser->name,
               'email' => $providerUser->email,
               'image' => $providerUser->avatar,
               'provider' => $driver,
               'provider_id' => $providerUser->id,
               'remember_token' => $providerUser->token,
               // user can use reset password to create a password
               'password' => ''
           ]);
           } else {
            $user = User::create([
                'name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'image' => $providerUser->getAvatar(),
                'provider' => $driver,
                'provider_id' => $providerUser->getId(),
                'access_token' => $providerUser->token,
                // user can use reset password to create a password
                'password' => ''
            ]);
        } }

        // login the user
        Auth::login($user, true);

        return $this->sendSuccessResponse();
    }

    /**
     * Check for provider allowed and services configured
     *
     * @param $driver
     * @return bool
     */
    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }
}
