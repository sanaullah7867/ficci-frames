<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Admin;
use App\Event_vistis;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;

use Illuminate\Support\Facades\Redirect;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    //use AuthenticatesUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/hall';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:juries');
    }

    public function showAdminRegisterForm()
    {
        return view('auth.register', ['url' => 'admin']);
    }


    protected function createAdmin(Request $request)
    {
        $this->validator($request->all())->validate();
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/admin');
    }



    protected function validator(array $data)
    {
			//print_r(strlen($data['contact_no'])); exit;
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'password' => ['required', 'string', 'min:6', 'confirmed'],
            'contact_no' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','digits:10','unique:users'],
            //'competition_id' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // print_r($data);
          //  exit;
              $user = User::create([
                 'name' => $data['name'],
                    'qualification' => $data['qualification'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['contact_no']),
                    'contact_no' =>$data['contact_no'],
                    'state' =>$data['state'],
                    'city' =>$data['city'],
                    'years_p' =>$data['no_of_years'],
                    'group_name' =>1,
                ]);

               
                $data1 = [
                    'name' 	=> $data['name'],
                    'email'=>$data['email'],
					'contact'=>$data['contact_no'],
                    'subject' => 'A',
                ];

               Mail::send('mails.otp', ['data1'=>$data1], function($message) use($data1){
                $message->from('info@eventregistration.in','ABC');
                     $message->subject($data1['subject']);
                       $message->to($data1['email']);
					  });
			 Session::flash('success','Register');
			return $user;
			
    }
	
	 public function check_user_mem()
    {
       $user_id=$_GET['user_id'];
       $type= $_GET['type'];
      if($type==1){
       $user = User::where('email',  $user_id)->first();
       $msg="Email ".$user_id." has already registered!!!";
    }
       if($type==2){
        $user = User::where('contact_no',  $user_id)->first(); 
            $msg="Mobile no. ".$user_id." has already registered!!!"; }
        if ($user) {
             return response()->json(['status' => "success", 'message' => $msg]);
        }
    }

    public function check_user()
    {
       $user_id=$_GET['user_id'];
      // $user_id= 'khan@abcdesigns.in';
      // $user_id= 'zakir@abcdesigns.in';
      Session::pull('user_email');
           Session::pull('user_id');
          Session::pull('token1');
          Session::pull('newuser');
        $user = User::where('email',  $user_id)->first();
        //print_r($user);
             if($user) {
                $token1 = Str::random(60);
                DB::table('users')
                ->where('email', $user_id)
                ->update(array('remember_token' => $token1));

                $otp = rand(10000,99999);


                $data = [
                    'name' 	=> $user->name,
                   // 'contact'        =>$request->contact,
                    'email'			=>$user->email,
                    'subject' 		=> 'OTP for reset Password',
                    'otp' 	=> $otp,

                ];

               Mail::send('mails.otp', ['data'=>$data], function($message) use($data){
                $message->from('zakir@abcdesigns.in','Volume Zero');
                     $message->subject($data['subject']);
                       $message->to($data['email']);
                        //$message->to($data['email']);
                    });
                    $otpget = Otps::where('otp', $otp)->where('email', $user->email)->first();
                    if($otpget){
                                DB::table('otps')
                            ->where('email', $user->email)
                            ->update(array('otp' => $otp, 'user_id'=>$user->id,'updated_at'=> Carbon::now()));
                     } else {
                            $obj = new Otps();
                            $obj->user_id = $user->id;
                            $obj->email = $user->email;
                            $obj->otp =$otp;
                            $obj->save();
                            $user_email=$user_id;
                            $user_id=$user->id;
                     }
                        if(isset($_GET['rst'])){

                        } else {
                            //return redirect()->intended('/signup');
                            session()->put('user_email',$user_email);
                            session()->put('user_id',$user_id);
                            session()->put('token1',$token1);
                            //return redirect('signup');
                            // return view('templates.reset_password',compact('user_email','user_id','token1'));
                            }

             } else {

                            $otp = rand(10000,99999);
                            $data = [
                                'name' 	=> "User",
                            // 'contact'        =>$request->contact,
                                'email'			=>$user_id,
                                'subject' 		=> 'Email Verification with OTP.',
                                'otp' 	=> $otp,
                            ];


                        Mail::send('mails.otp', ['data'=>$data], function($message) use($data){
                            $message->from('competition@volzero.com','Volume Zero');
                                $message->subject($data['subject']);
                                $message->to($data['email']);
                                    //$message->to($data['email']);
                                });
                                $otpget = Otps::where('otp', $otp)->where('email', $user_id)->first();
                                if($otpget){
                                            DB::table('otps')
                                        ->where('email', $user_id)
                                        ->update(array('otp' => $otp, 'updated_at'=> Carbon::now()));
                                } else {
                                        $obj = new Otps();
                                        $obj->email = $user_id;
                                        $obj->otp =$otp;
                                        $obj->save();
                                        $user_email=$user_id;

                                }

                            session()->put('newuser',$user_email);
                            //session()->put('newuseremail',$user_email);
                            //return redirect('signup');
                            //return view('ajax_pages.exist_user',compact('user_id'));}
                        }
}

    public function verify_otp()
    {       // email is user_id
        $otp=$_GET['otp'];
       $email =$_GET['user_id'];
       $user = Otps::where('otp', $otp)->where('email', $email)->first();
        if ($user) {

                         return response()->json(['status' => "success", 'message' => "Otp verified."]);
                    }
              else {
                    return response()->json(['status' => "fail", 'message' => $otp. " is Invailid!!! Please enter correct OTP." ]);
               }
      }

      public static function check_user_email()
      {
        $user = User::where('email',  $_GET['email_id'])->first();
        if ($user) {
            return response()->json(['status' => "success"]);
        }
        else {
        return response()->json(['status' => "fail"]);
        }
    }

    public static function check_userdetails()
    {

       // Auth::logout();
       // Session::flush();
        if(Auth::attempt(['email' => $_GET['email_id'], 'password' => $_GET['psw']])){
        User::where('id',Auth::user()->id)->update(array('login_at' => Carbon::now()));
                
          return response()->json(['status' => "success"]);
      }
      else {
      return response()->json(['status' => "fail"]);
      }
  }
}

