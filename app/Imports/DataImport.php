<?php

namespace App\Imports;
use App\User;
use App\Address_others;
use App\Apply_promos;
use App\Competitions;
use App\Countries;
use App\email_templates;
use App\Enquiries;
use App\Groups;
use App\Juries;
use App\Occupations;
use App\Participant_as;
use App\Participants;
use App\Payment_gatways;
use App\payment_statuses;
use App\Promo_codes;
use App\Results;
use App\States;
use App\subscriptions;
use App\Total_scores;
use App\Uploads;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class DataImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        return new User([
            'name'     => $row['name'],
            'email'    => $row['email'], 
            'password' => \Hash::make($row['password']),
        ]);
    }
}
