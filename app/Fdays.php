<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fdays extends Model
{
    protected $fillable = [
        'regid','fdays',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
