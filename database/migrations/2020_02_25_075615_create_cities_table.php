<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if (!Schema::hasTable('cities')) {
        Schema::create('cities', function (Blueprint $table) {
         $table->bigIncrements('city_id');
			$table->char('name',150);
			$table->integer('state_id')->usigned();;
			$table->foreign('state_id')->references('state_id')->on('states');
         });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
