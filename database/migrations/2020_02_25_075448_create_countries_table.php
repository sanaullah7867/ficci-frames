<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if (!Schema::hasTable('countries')) {
			Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('county_id');
			$table->char('iso',2)->nullable();
			$table->char('name',150);
			$table->char('nicename',150)->nullable();
			$table->char('iso3',3)->nullable();
			$table->integer('numcode')->nullable();
			$table->integer('phonecode')->nullable();
         });
		}
	}	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
