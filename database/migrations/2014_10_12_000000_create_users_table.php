<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if (!Schema::hasTable('users')) {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
			$table->integer("competition_id")->usigned();
            $table->string('name');
            $table->string('email')->unique();
			$table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string("contact_no",25);
            $table->date("DOB");
			$table->integer("occupation_id")->usigned();
			$table->string('profile_pic',150)->nullable();
			$table->string('reference',50)->nullable();
			$table->string('other_ref',80)->nullable();
			 $table->rememberToken();
            $table->timestamps();
			$table->foreign('competition_id')->references('competition_id')->on('competitions');
			$table->foreign('occupation_id')->references('occupation_id')->on('occupations');
        });
    }
	}
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
